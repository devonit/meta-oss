DESCRIPTION = "REALTIMEKIT Realtime Policy and Watchdog Daemon"
HOMEPAGE = "git://git.0pointer.de/rtkit.git"
SECTION =  "libs"
PRIORITY = "optional"
LICENSE = "GPLv3"
LIC_FILES_CHKSUM = "file://LICENSE;md5=4908485e91c489687356ed04359bf522"

PR = "r4"
DEPENDS += "dbus systemd"
SRCREV = "v${PV}"
SRC_URI = " \
             git://git.0pointer.de/rtkit.git \
             file://link_with_librt.patch \
             file://rtkit-controlgroup.patch \
          "
S = "${WORKDIR}/git"

inherit autotools pkgconfig gettext systemd useradd

RPROVIDES_${PN} += "${PN}-systemd"
RREPLACES_${PN} += "${PN}-systemd"
RCONFLICTS_${PN} += "${PN}-systemd"
SYSTEMD_SERVICE_${PN} = "rtkit-daemon.service"

USERADD_PACKAGES = "${PN}"
USERADD_PARAM_${PN} = "--system -u 172 -g rtkit -d /proc -s /sbin/nologin rtkit"
GROUPADD_PARAM_${PN} = "-r -g 172 rtkit"

FILES_${PN} += " \
                 /usr/share/polkit-1/actions/org.freedesktop.RealtimeKit1.policy \
                 /usr/share/dbus-1/system-services/org.freedesktop.RealtimeKit1.service \
               "
