pkg_postinst_udev-hwdb_append () {
    chown root:root $D/${sysconfdir}/udev/hwdb.bin
}

FILESEXTRAPATHS_prepend := "${THISDIR}/${PN}:"
SRC_URI += "\
    file://60-keyboard.rules \
    file://ldconfig.service \
"

do_install_append () {
    install -d ${D}/${rootlibexecdir}/udev/rules.d/
    install -m 644 ${WORKDIR}/60-keyboard.rules ${D}/${rootlibexecdir}/udev/rules.d/60-keyboard.rules

    # install ldconfig.service and distro activation symlink
    install -m 644 ${WORKDIR}/ldconfig.service ${D}${systemd_unitdir}/system/ldconfig.service
    ln -sf ${systemd_unitdir}/system/ldconfig.service \
           ${D}/${systemd_unitdir}/system/sysinit.target.wants/ldconfig.service

    #Remove tmp.mount. It was starting too late and we don't need it.
    rm -rf ${D}/${systemd_unitdir}/system/tmp.mount
    rm -rf ${D}/${systemd_unitdir}/system/local-fs.target.wants/tmp.mount
}
