DESCRIPTION = "Tofrodos is a text file conversion utility that converts ASCII files between the MSDOS and unix format"
LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://../COPYING;md5=8ca43cbc842c2336e835926c2166c28b"
PR = "r2"

SRC_URI = "http://tofrodos.sourceforge.net/download/tofrodos-${PV}.tar.gz \
           file://cross.patch;striplevel=2"

S = "${WORKDIR}/${PN}/src"

do_install() {
	install -d ${D}/usr/man/man1
	install -d ${D}/${bindir}
	oe_runmake DESTDIR=${D} install
	cd ${D}/${bindir}
}

FILES_${PN} = "${bindir}/fromdos ${bindir}/todos"

pkg_postinst_${PN} () {
#!/bin/sh
    update-alternatives --install ${bindir}/dos2unix dos2unix fromdos 100
    update-alternatives --install ${bindir}/unix2dos unix2dos todos 100
}

pkg_prerm_${PN} () {
 #!/bin/sh
   update-alternatives --remove dos2unix fromdos
   update-alternatives --remove unix2dos todos
}

SRC_URI[md5sum] = "c4c5e6668a13a01bfb5ce562753a808f"
SRC_URI[sha256sum] = "3457f6f3e47dd8c6704049cef81cb0c5a35cc32df9fe800b5fbb470804f0885f"


