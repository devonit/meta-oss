# We need to tell libsndfile to use 64-bit addressing, recent versions lock this behavior in.
# this is the same thing the existing recipe uses for ARM, we're just adding a second case.

do_configure_prepend() {
    export ac_cv_sys_largefile_source=1
    export ac_cv_sys_file_offset_bits=64
    ac_cv_sizeof_off_t=8
}

