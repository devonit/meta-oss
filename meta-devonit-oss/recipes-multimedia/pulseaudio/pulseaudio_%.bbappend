FILESEXTRAPATHS_prepend := "${THISDIR}/${PN}:"

SRC_URI += " \
              file://pulseaudio.start \
              file://71pulse-select-usb \
           "

DEPENDS += " \
             fftwf \
           "

EXTRA_OECONF := "${@oe_filter_out('--without-fftw', '${EXTRA_OECONF}', d)}"
EXTRA_OECONF += "--with-fftw"

RDEPENDS_pulseaudio-server := "${@oe_filter_out('pulseaudio-module-console-kit', '${RDEPENDS_pulseaudio-server}', d)}"

RDEPENDS_pulseaudio-server += " \
    pulseaudio-module-switch-on-connect        \
    pulseaudio-module-switch-on-port-available        \
    pulseaudio-module-cli \
    pulseaudio-module-cli-protocol-unix \
    pulseaudio-module-mmkbd-evdev \
    pulseaudio-module-volume-restore \
    pulseaudio-module-rescue-streams \
    pulseaudio-module-always-sink \
   "


do_install_append() {
    rm -f ${D}${sysconfdir}/xdg/autostart/pulseaudio.desktop
    rm -f ${D}${sysconfdir}/xdg/autostart/pulseaudio-kde.desktop

    install -d ${D}${sysconfdir}/X11/Xsession.d/
    install -m 0755 ${WORKDIR}/pulseaudio.start ${D}${sysconfdir}/X11/Xsession.d/70pulseaudio

    #fixup daemon.conf
    sed -i "/realtime-scheduling = /c\realtime-scheduling = yes" ${D}${sysconfdir}/pulse/daemon.conf
    sed -i "/exit-idle-time = /c\exit-idle-time = -1" ${D}${sysconfdir}/pulse/daemon.conf
    sed -i "/resample-method = /c\resample-method = speex-float-1" ${D}${sysconfdir}/pulse/daemon.conf
    sed -i "/flat-volumes = /c\flat-volumes = no" ${D}${sysconfdir}/pulse/daemon.conf
    sed -i "/default-fragments = /c\default-fragments = 8" ${D}${sysconfdir}/pulse/daemon.conf
    sed -i "/default-fragment-size-msec = /c\default-fragment-size-msec = 10" ${D}${sysconfdir}/pulse/daemon.conf
    sed -i "/resample-method = /c\resample-method = trivial" ${D}${sysconfdir}/pulse/daemon.conf

    #fixup default.pa
    sed -i "/module-suspend-on-idle/c\load-module module-suspend-on-idle timeout=0" ${D}${sysconfdir}/pulse/default.pa

    #insert switch-on-connect module
    echo -en "\n### Switch on connect\nload-module module-switch-on-connect" >> ${D}${sysconfdir}/pulse/default.pa
    install -m 0755 ${WORKDIR}/71pulse-select-usb ${D}${sysconfdir}/X11/Xsession.d/71pulse-select-usb

}

FILES_${PN} += " \
    ${sysconfdir}/X11/Xsession.d/70pulseaudio \
    ${sysconfdir}/X11/Xsession.d/71pulse-select-usb \
    "

