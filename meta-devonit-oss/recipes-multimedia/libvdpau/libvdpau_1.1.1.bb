SUMMARY = "Video Decode and Presentation API for Unix"
DESCRIPTION = "A non-proprietary and royalty-free open source library \
               and API targeted at the X Window System. This allows video \
               programs to offload portions of the video decoding process \
               and video post-processing to the GPU video hardware."

HOMEPAGE = "http://www.freedesktop.org/wiki/Software/VDPAU/"
BUGTRACKER = "https://bugs.freedesktop.org"

SECTION = "x11"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://COPYING;md5=83af8811a28727a13f04132cc33b7f58"

PR = "r2"
SRCREV = "libvdpau-${PV}"
DEPENDS = "libxext libdrm dri2proto"

inherit autotools pkgconfig

SRC_URI = "git://people.freedesktop.org/~aplattner/libvdpau"
S = "${WORKDIR}/git"

PACKAGES =+ "${PN}-trace ${PN}-trace-dbg"
FILES_${PN}-dev += "${libdir}/vdpau/libvdpau_trace.la ${libdir}/vdpau/libvdpau_*${SOLIBSDEV}"
FILES_libvdpau-trace = "${libdir}/vdpau/libvdpau_*${SOLIBS}"
FILES_libvdpau-trace-dbg = "${libdir}/vdpau/.debug/*"
