SUMMARY = "VA driver for VDPAU backends"

HOMEPAGE = "http://www.freedesktop.org/wiki/Software/VDPAU"
BUGTRACKER = "https://bugs.freedesktop.org"

LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://COPYING;md5=751419260aa954499f7abaabaa882bbe"

DEPENDS = "libva libvdpau"

PR = "r1"
SRCREV = "${PV}"

inherit autotools pkgconfig

SRC_URI = " \
             git://anongit.freedesktop.org/vaapi/vdpau-driver \
             file://0001-VAEncH264VUIBufferType-and-VAEncH264SEIBufferType-ar.patch \
             file://build_with_recent_glext.patch \
          "

S = "${WORKDIR}/git"
# The .so symlinks are to keep the vaapi loader happy
INSANE_SKIP_${PN} = "dev-so"
FILES_${PN} += "${libdir}/dri/*.so"
FILES_${PN}-dbg += "${libdir}/dri/.debug"
FILES_${PN}-dev += "${libdir}/dri/*.la"
