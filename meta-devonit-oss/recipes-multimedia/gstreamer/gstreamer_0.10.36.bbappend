# Add the l/loop option to gst-launch, so that you can loop without rebuilding the pipeline.
FILESEXTRAPATHS_prepend := "${THISDIR}/${PN}:"

SRC_URI += " \
	file://0001-gst-launch-add-loop.patch \
"
