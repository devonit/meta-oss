PACKAGES = "${PN}-docs ${PN} ${PN}-dev ${PN}-dbg ${PN}-cups ${PN}-locale ${PN}-staticdev"

FILES_${PN}-docs += "${datadir}/${PN}/${PV}/doc/* ${datadir}/man"
FILES_${PN}-dev += "${datadir}/${PN}/${PV}/examples/*"
