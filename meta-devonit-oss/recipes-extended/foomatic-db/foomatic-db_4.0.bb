DESCRIPTION = "Foomatic database"
LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://COPYING;md5=89aead728f64ad2af95c03f5793274bc"

inherit autotools

PR = "r10"
REV = "1288"

SRC_URI = " \
    http://downloads.devonit.com/Development/accounts/development/sources/vendor/foomatic/${PN}-${PV}-${REV}.tar.bz2 \
"

B = "${S}"
S = "${WORKDIR}/${PN}-${REV}"

RDEPENDS_${PN} += "ghostscript poppler"

do_install_append () {
    install -d ${D}/usr/share/cups/model/
    
    # symlink the foomatic ppds to the location cups wants
    ln -s /usr/share/foomatic/db/source/PPD/ ${D}/usr/share/cups/model/foomatic-db-ppds

    chown -R root:root "${D}"
}

PACKAGES += " \
              ${PN}-ppd-epson \
              ${PN}-ppd-infoprint \
              ${PN}-ppd-samsung \
              ${PN}-ppd-oce \
              ${PN}-ppd-toshiba \
              ${PN}-ppd-sharp \
              ${PN}-ppd-lanier \
              ${PN}-ppd-kyocera \
              ${PN}-ppd-nrg \
              ${PN}-ppd-infotec \
              ${PN}-ppd-hp \
              ${PN}-ppd-savin \
              ${PN}-ppd-gestetner \
              ${PN}-ppd-brother \
              ${PN}-ppd-ricoh \
              ${PN}-ppd-lexmark \
              ${PN}-ppd-oki \
              ${PN}-ppd-konica-minolta \
            "

FILES_${PN}-ppd-epson = "/usr/share/foomatic/db/source/PPD/Epson"
FILES_${PN}-ppd-infoprint  = "/usr/share/foomatic/db/source/PPD/InfoPrint"
FILES_${PN}-ppd-samsung = "/usr/share/foomatic/db/source/PPD/Samsung"
FILES_${PN}-ppd-oce = "/usr/share/foomatic/db/source/PPD/Oce"
FILES_${PN}-ppd-toshiba = "/usr/share/foomatic/db/source/PPD/Toshiba"
FILES_${PN}-ppd-sharp = "/usr/share/foomatic/db/source/PPD/Sharp"
FILES_${PN}-ppd-lanier = "/usr/share/foomatic/db/source/PPD/Lanier"
FILES_${PN}-ppd-kyocera = "/usr/share/foomatic/db/source/PPD/Kyocera"
FILES_${PN}-ppd-nrg = "/usr/share/foomatic/db/source/PPD/NRG"
FILES_${PN}-ppd-infotec = "/usr/share/foomatic/db/source/PPD/Infotec"
FILES_${PN}-ppd-hp = "/usr/share/foomatic/db/source/PPD/HP"
FILES_${PN}-ppd-savin = "/usr/share/foomatic/db/source/PPD/Savin"
FILES_${PN}-ppd-gestetner = "/usr/share/foomatic/db/source/PPD/Gestetner"
FILES_${PN}-ppd-brother = "/usr/share/foomatic/db/source/PPD/Brother"
FILES_${PN}-ppd-ricoh = "/usr/share/foomatic/db/source/PPD/Ricoh"
FILES_${PN}-ppd-lexmark = "/usr/share/foomatic/db/source/PPD/Lexmark"
FILES_${PN}-ppd-oki = "/usr/share/foomatic/db/source/PPD/Oki"
FILES_${PN}-ppd-konica-minolta = "/usr/share/foomatic/db/source/PPD/KONICA_MINOLTA"

FILES_${PN} = " \
    /usr/share/foomatic/templates \
    /usr/share/foomatic/xmlschema \
    /usr/share/foomatic/db/oldprinterids \
    /usr/share/foomatic/db/source/driver \
    /usr/share/foomatic/db/source/opt \
    /usr/share/foomatic/db/source/printer \
    /usr/share/cups \
"

SRC_URI[md5sum] = "cef610d41f704d6c94bcf76fd8c9dd64"
SRC_URI[sha256sum] = "a5676138677c925e9c3c7b250f53edae156d4ca291b6bc317a77f1a08980db5e"
