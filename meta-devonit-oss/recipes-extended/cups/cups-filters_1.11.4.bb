SUMMARY = "Filters for CUPS"
DESCRIPTION = "As of CUPS 1.6 a lot of filters are no longer provided in the default \
               CUPS source such as texttops. This adds them back in."

inherit autotools pkgconfig

DEPENDS += "cups poppler freetype fontconfig ijs qpdf"
RDEPENDS_${PN} += "qpdf"

PR = "r5"
B = "${S}"

LICENSE = "GPLv3"
LIC_FILES_CHKSUM = "file://NEWS;md5=17bc5972c51f2217ed367fde32967204"

SRC_URI += "http://openprinting.org/download/cups-filters/cups-filters-${PV}.tar.bz2"

SRC_URI[md5sum] = "2261ef8250fb3e9870cb6c4905e73b01"
SRC_URI[sha256sum] = "84da33de13d9018bfe1d42cda4fcbae8243106bfaa18cb885c9793d01a9306a7"

EXTRA_OECONF += " \
               --disable-avahi \
               --enable-dbus \
               --without-tiff \
               --without-php \
               --without-jpeg \  
               "

FILES_${PN}-dbg += "/usr/lib/cups/filter/.debug \
                    /usr/lib/cups/backend/.debug \
                   "
FILES_${PN} += "/usr/lib/cups/backend \
                /usr/lib/cups/filter \
                /usr/share/cups \
                /usr/share/ppd \
                /usr/bin \
                "
