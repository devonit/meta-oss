DESCRIPTION = "Foomatic database engine"
LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://COPYING;md5=393a5ca445f6965873eca0259a17f833"

inherit autotools pkgconfig

PR = "r4"

REV = "371"

SRC_URI = " \
    http://downloads.devonit.com/Development/accounts/development/sources/vendor/foomatic/${PN}-${PV}-${REV}.tar.bz2 \
"

B = "${S}"
S = "${WORKDIR}/${PN}-${REV}"

DEPENDS += "libxml2"
RDEPENDS_${PN} += "perl-modules"

TARGET_CC_ARCH += "${LDFLAGS}"

do_configure_prepend() {
    export LIBDIR=/usr/share/foomatic
    export PERL_INSTALLDIRS=vendor
}

do_install_append() {
    install -d ${D}/usr/lib/cups/driver/
    ln -s /usr/bin/foomatic-ppdfile ${D}/usr/lib/cups/driver/
}

FILES_${PN} += " \
    ${libdir}/cups/driver/foomatic \
    ${exec_prefix}/share/foomatic/templates/* \
    ${bindir}/* \
    ${sbindir}/* \
    /usr/lib/cups/driver/ \
    /usr/share/perl5 \
    /usr/lib/perl \
    /etc/foomatic \
"

SRC_URI[md5sum] = "4ef51d0a4d9bc9cb78c0eed8a0368458"
SRC_URI[sha256sum] = "27a0bd7a70a90ead585ce1c105855ef70c892a1a1bf9f6a6864d85cf43af1b8c"
