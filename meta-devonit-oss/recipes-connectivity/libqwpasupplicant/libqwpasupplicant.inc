DESCRIPTION = "A nice Qt wrapper around the wpa_supplicant dbus api"
LICENSE = "LGPLv2.1"
LIC_FILES_CHKSUM = "file://${COREBASE}/meta/files/common-licenses/LGPL-3.0;md5=bfccfe952269fff2b407dd11f2f3083b"

inherit qt4x11

RDEPENDS_${PN} += " \
    wpa-supplicant \
    dbus \
    "

QMAKE_PROFILES = "qwpasupplicant.pro"

PR = "r1"
SRCREV ?= "v${PV}"

SRC_URI = " \
    git://bitbucket.org/devonit/qwpasupplicant.git;protocol=https \
    "

S = "${WORKDIR}/git"

EXTRA_QMAKEVARS_PRE += "PREFIX=/usr"

do_install () {
    oe_runmake install INSTALL_ROOT="${D}"
}

