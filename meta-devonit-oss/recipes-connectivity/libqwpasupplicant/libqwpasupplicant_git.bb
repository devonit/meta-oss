include recipes-connectivity/libqwpasupplicant/libqwpasupplicant.inc
inherit gitpkgv

SRCREV = "${AUTOREV}"
DEFAULT_PREFERENCE = "-1"

PKGV = "0.1.0+gitr${GITPKGV}"


do_install_append () {
    sed 's#/mnt/bitbake/build/detos/tmp-glibc/sysroots/x86_64-linux##' -i ${D}/${libdir}/pkgconfig/*.pc
}
