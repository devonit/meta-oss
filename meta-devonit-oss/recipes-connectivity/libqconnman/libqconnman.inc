DESCRIPTION = "A nice Qt wrapper around the connman api"
LICENSE = "LGPLv2.1"
LIC_FILES_CHKSUM = "file://${COREBASE}/meta/files/common-licenses/LGPL-3.0;md5=bfccfe952269fff2b407dd11f2f3083b"

inherit qt4x11

QMAKE_PROFILES = "qconnman.pro"
RDEPENDS_${PV} += "connman"

PR = "r1"
SRCREV ?= "v${PV}"

SRC_URI = " \
            git://bitbucket.org/devonit/qconnman.git;protocol=https \
          "

S = "${WORKDIR}/git"

EXTRA_QMAKEVARS_PRE += "PREFIX=/usr"

do_install () {
    oe_runmake install INSTALL_ROOT="${D}"
    install -d ${D}/opt/qconnman/bin
    install -m 755 ${S}/examples/list-services/list-services ${D}/opt/qconnman/bin/
    install -m 755 ${S}/examples/openconnect-vpnd/openconnect-vpnd ${D}/opt/qconnman/bin/
    install -m 755 ${S}/examples/test-vpnmanager/test-vpnmanager ${D}/opt/qconnman/bin/
    
}

FILES_${PN} += "/usr"
FILES_${PN}-dbg += "/opt/qconnman/bin \
                    /usr/lib/*/*/*/QConnman/.debug/*.so \
                    /usr/share/tests/tst_serviceconfiguration/.debug/* \
                   "
