include recipes-connectivity/libqconnman/libqconnman.inc
inherit qt4x11

#fix pkgconfig for the sysroot
do_install_append () {
    sed 's#/mnt/bitbake/build/detos/tmp-glibc/sysroots/x86_64-linux##' -i ${D}/${libdir}/pkgconfig/*.pc
}
