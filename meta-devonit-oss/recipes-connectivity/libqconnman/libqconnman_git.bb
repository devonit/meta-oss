include recipes-connectivity/libqconnman/libqconnman.inc
inherit gitpkgv

SRCREV = "${AUTOREV}"
DEFAULT_PREFERENCE = "-1"

PKGV = "1.24+gitr${GITPKGV}"
