DESCRIPTION = "OpenConnect is a client for Cisco's AnyConnect SSL VPN, which is supported by the ASA5500 Series, by IOS 12.4(9)T or later on Cisco SR500, 870, 880, 1800, 2800, 3800, 7200 Series and Cisco 7301 Routers, and probably others."
HOMEPAGE = "http://www.infradead.org/openconnect/index.html"
LICENSE = "LGPLv2.1"
LIC_FILES_CHKSUM = "file://COPYING.LGPL;md5=243b725d71bb5df4a1e5920b344b86ad"

RDEPENDS_${PN} += "vpnc"
PR = "2"

DEPENDS += "openssl gnutls vpnc libxml2"

EXTRA_OECONF += "--with-vpnc-script=/etc/vpnc/vpnc-script"
# we have the special cisco patch to openssl
EXTRA_OECONF += "--without-openssl-version-check"

SRC_URI = "ftp://ftp.infradead.org/pub/openconnect/${PN}-${PV}.tar.gz"
SRC_URI[md5sum] = "8af1306ac4af7b85b6c08a1a4d216014"
SRC_URI[sha256sum] = "243e7d810776353e097a82849ebc69336a987c17478acd9f1ee68cf604b1edf8"

inherit autotools pkgconfig

