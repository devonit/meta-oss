DEPENDS += "perl-native"
DEPENDS_append_class-target = " openssl-native"

CFLAG = "${@base_conditional('SITEINFO_ENDIANNESS', 'le', '-DL_ENDIAN', '-DB_ENDIAN', d)} \
	-DTERMIO ${CFLAGS} -Wall -Wa,--noexecstack"

# -02 does not work on mipsel: ssh hangs when it tries to read /dev/urandom
CFLAG_mtx-1 := "${@'${CFLAG}'.replace('-O2', '')}"
CFLAG_mtx-2 := "${@'${CFLAG}'.replace('-O2', '')}"

PACKAGECONFIG += "perl"
do_install_append () {
    # make ssl/certs a symlink to /etc/ssl/certs (which is a symlink to /usr/share/ca-certificates)
    rm -rf ${D}${libdir}/ssl/certs
    ln -s /etc/ssl/certs ${D}${libdir}/ssl/certs
    # symlink ca-certificates' ca-certificates chain to openssl's cert.pem (root certs baby)
    rm -f ${D}${libdir}/ssl/cert.pem
    ln -s /etc/ssl/certs/ca-certificates.crt ${D}${libdir}/ssl/cert.pem   
}
