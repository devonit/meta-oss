# Copyright (C) 2016 Devon IT
# Released under the MIT license (see COPYING.MIT for the terms)

DESCRIPTION = "CertNanny SSCEP, a command line client for the SCEP protocol"
HOMEPAGE = "SSCEP is a command line client for the SCEP protocol"
LICENSE = "MIT"
DEPENDS = "zlib openssl"
PR = "r2"

LIC_FILES_CHKSUM = "\
    file://COPYRIGHT;md5=f07b700e0837dbf9e8a703a56de7ee4b \
    "

SRC_URI = "\
    git://github.com/certnanny/sscep.git;protocol=https;tag=v${PV} \
    file://mkrequest-string_mask.patch \
    file://makefile.patch \
    "

S = "${WORKDIR}/git"

# There is no configure script
do_configure() {
    pushd ${S}
    cp Linux/Makefile Makefile
    popd
}

do_compile() {
    oe_runmake sscep_dyn \
        CC="${CC}" \
        CFLAGS="${CFLAGS}" \
        LDFLAGS="${LDFLAGS}"
}

do_install() {
    install -Dm755 ${S}/sscep_dyn   ${D}/${sbindir}/sscep
    install -Dm755 ${S}/mkrequest   ${D}/${sbindir}/sscep-makerequest
    install -Dm644 ${S}/README      ${D}/usr/share/doc/sscep/README
    install -Dm644 ${S}/COPYRIGHT   ${D}/usr/share/licenses/sscep/COPYRIGHT
}

FILES_${PN} = "\
    ${sbindir}/sscep \
    ${sbindir}/sscep-makerequest \
    /usr/share/doc/sscep \
    /usr/share/licenses/sscep \
    "

