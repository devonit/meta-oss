FILESEXTRAPATHS_prepend := "${THISDIR}/${PN}:"


SRC_URI += " \
    file://main.conf \
    file://settings.config \
    file://disable_dns_cache.conf \
    file://0001-make-dhcp-request-timeout-longer.patch \
    file://0001-disable-ipv6-support-by-default.patch \
    file://0001-autoscan-timeout.patch \
    file://0001-always-autoconnect.patch \
    "

PACKAGECONFIG_append += "openvpn vpnc l2tp pptp openconnect"

EXTRA_OECONF += " \
    --enable-openconnect \
    --with-openconnect=${sbindir}/openconnect \
"

PACKAGES =+ "${PN}-plugin-vpn-openconnect"
RDEPENDS_${PN}-plugin-vpn-openconnect += "${PN}-vpn openconnect"
RRECOMMENDS_${PN} += " \
    ${PN}-plugin-vpn-openconnect \
    ${PN}-plugin-vpn-pptp \
    ${PN}-plugin-vpn-l2tp \
"

do_install_append () {
    install -d ${D}/${sysconfdir}/connman
    install -m 0640 ${WORKDIR}/main.conf ${D}/${sysconfdir}/connman/main.conf
    install -d ${D}${localstatedir}/lib/connman
    install -m 0644 ${WORKDIR}/settings.config ${D}${localstatedir}/lib/connman/settings
    install -d ${D}/${sysconfdir}/systemd/system/connman.service.d
    install -m 0640 ${WORKDIR}/disable_dns_cache.conf ${D}/${sysconfdir}/systemd/system/connman.service.d
}

FILES_${PN} += " \
    ${sysconfdir}/connman/main.conf \
    ${sysconfdir}/systemd/system/connman.service.d \
"

FILES_${PN}-plugin-vpn-openconnect += " \
    ${libdir}/connman/scripts/openconnect-script \
    ${libdir}/connman/plugins-vpn/openconnect.so \
"

