# append XFWM bug 12587 to the 4.12.3 recipe
FILESEXTRAPATHS_prepend := "${THISDIR}/${PN}-${PV}:"

SRC_URI += "\
    file://xfwm4-bug-12587.patch \
    "
