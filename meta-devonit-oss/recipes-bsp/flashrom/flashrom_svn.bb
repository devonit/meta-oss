DESCRIPTION = "Flashrom is a simple utility for reading/writing BIOS chips"
HOMEPAGE = "http://www.coreboot.org/Flashrom"
LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://COPYING;md5=751419260aa954499f7abaabaa882bbe"
DEPENDS = "zlib pciutils"

SRCREV = "1315"
PV = "0.0+svnr${SRCPV}"
PR = "r0"

SRC_URI = " \
	svn://coreboot.org/flashrom;module=trunk \
	"

S = "${WORKDIR}/trunk"

LDFLAGS += "-lpci -lz"

do_install () {
	install -d "${D}${sbindir}"
	oe_runmake 'PREFIX=${D}/${prefix}' install
}




