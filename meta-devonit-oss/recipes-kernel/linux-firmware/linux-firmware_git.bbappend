
SRCREV = "a179db97914da5e650c21ba8f9b0bae04a0f8a41"

LIC_FILES_CHKSUM = "\
    file://LICENSE.radeon;md5=69612f4f7b141a97659cb1d609a1bde2 \
    file://LICENSE.amdgpu;md5=3fe8a3430700a518990c3b3d75297209 \
    "

PACKAGES =+ " \
    ${PN}-keyspan \
    ${PN}-btintel \
    ${PN}-r8192e-pci \
    ${PN}-r8169 \
    ${PN}-tg3 \
    ${PN}-rt2800pci  \
    ${PN}-rt61pci  \
    ${PN}-rt73usb  \
    ${PN}-rtl8188ee  \
    ${PN}-rtl8192de  \
    ${PN}-rtl8192ee  \
    ${PN}-rtl8192se  \
    ${PN}-rtl8723ae  \
    ${PN}-rtl8723be  \
    ${PN}-rtl8821ae \
    ${PN}-rtl8xxxu  \
    ${PN}-r8723au \
    ${PN}-nouveau \
    ${PN}-ath9k-htc \
    "
            
FILES_${PN}-keyspan = " \
    /lib/firmware/keyspan/usa49wlc.fw \ 
    /lib/firmware/keyspan/usa49w.fw \
    /lib/firmware/keyspan/usa19w.fw \
    /lib/firmware/keyspan/usa18x.fw \
    /lib/firmware/keyspan/usa19qw.fw \
    /lib/firmware/keyspan/mpr.fw \
    /lib/firmware/keyspan/usa19qi.fw \
    /lib/firmware/keyspan/usa19.fw \
    /lib/firmware/keyspan/usa28xb.fw \
    /lib/firmware/keyspan/usa28xa.fw \
    /lib/firmware/keyspan/usa28x.fw \
    /lib/firmware/keyspan/usa28.fw \
    "


FILES_${PN}-btintel = " \
    /lib/firmware/intel/ibt-11-5.ddc \
    /lib/firmware/intel/ibt-11-5.sfi \
    "

FILES_${PN}-r8192e-pci = " \
    /lib/firmware/RTL8192E/data.img \
    /lib/firmware/RTL8192E/main.img \
    /lib/firmware/RTL8192E/boot.img \
    "
                            
FILES_${PN}-r8169 = " \
    /lib/firmware/rtl_nic/rtl8107e-2.fw \
    /lib/firmware/rtl_nic/rtl8107e-1.fw \
    /lib/firmware/rtl_nic/rtl8168h-2.fw \
    /lib/firmware/rtl_nic/rtl8168h-1.fw \
    /lib/firmware/rtl_nic/rtl8168g-3.fw \
    /lib/firmware/rtl_nic/rtl8168g-2.fw \
    /lib/firmware/rtl_nic/rtl8106e-2.fw \
    /lib/firmware/rtl_nic/rtl8106e-1.fw \
    /lib/firmware/rtl_nic/rtl8411-2.fw \
    /lib/firmware/rtl_nic/rtl8411-1.fw \
    /lib/firmware/rtl_nic/rtl8402-1.fw \
    /lib/firmware/rtl_nic/rtl8168f-2.fw \
    /lib/firmware/rtl_nic/rtl8168f-1.fw \
    /lib/firmware/rtl_nic/rtl8105e-1.fw \
    /lib/firmware/rtl_nic/rtl8168e-3.fw \
    /lib/firmware/rtl_nic/rtl8168e-2.fw \
    /lib/firmware/rtl_nic/rtl8168e-1.fw \
    /lib/firmware/rtl_nic/rtl8168d-2.fw \
    /lib/firmware/rtl_nic/rtl8168d-1.fw \
    "

FILES_${PN}-tg3 = " \
    /lib/firmware/tigon/tg3_tso5.bin \
    /lib/firmware/tigon/tg3_tso.bin \
    /lib/firmware/tigon/tg3.bin \
    "
                     
FILES_${PN}-rt2800pci = "\
    /lib/firmware/rt2860.bin \
    "
                        
FILES_${PN}-rt2800usb = " \
    /lib/firmware/rt2870.bin \
    "
                        
FILES_${PN}-rt61pci = "\
    /lib/firmware/rt2661.bin \
    /lib/firmware/rt2561s.bin \
    /lib/firmware/rt2561.bin \
    "
                      
                     
FILES_${PN}-rt73usb = "\
    /lib/firmware/rt73.bin \
    "

FILES_${PN}-rtl8188ee = "\
/lib/firmware/rtlwifi/rtl8188efw.bin \
"

FILES_${PN}-rtl8192de = "\
    /lib/firmware/rtlwifi/rtl8192defw.bin \
    "

FILES_${PN}-rtl8192ee = "\
    /lib/firmware/rtlwifi/rtl8192eefw.bin \
    "

FILES_${PN}-rtl8192se = "\
    /lib/firmware/rtlwifi/rtl8192sefw.bin \
    "

FILES_${PN}-rtl8723ae = "\
    /lib/firmware/rtlwifi/rtl8723fw_B.bin \
    "

FILES_${PN}-rtl8723be = "\
    /lib/firmware/rtlwifi/rtl8723befw.bin \
    "

FILES_${PN}-rtl8821ae = "\
    /lib/firmware/rtlwifi/rtl8821aefw.bin \
    /lib/firmware/rtlwifi/rtl8821aefw_wowlan.bin \
    "

FILES_${PN}-rtl8xxxu = "\
    /lib/firmware/rtlwifi/rtl8723bu_bt.bin \
    /lib/firmware/rtlwifi/rtl8723bu_nic.bin \
    /lib/firmware/rtlwifi/rtl8192eu_nic.bin \
    /lib/firmware/rtlwifi/rtl8192cufw_TMSC.bin \
    /lib/firmware/rtlwifi/rtl8192cufw_B.bin \
    /lib/firmware/rtlwifi/rtl8192cufw_A.bin \
    "

FILES_${PN}-r8723au = "\
    /lib/firmware/rtlwifi/rtl8723aufw_B_NoBT.bin \
    /lib/firmware/rtlwifi/rtl8723aufw_B.bin \
    /lib/firmware/rtlwifi/rtl8723aufw_A.bin \
    "

FILES_${PN}-nouveau = " \
    /lib/firmware/nvidia/gm206/gr/sw_method_init.bin \
    /lib/firmware/nvidia/gm206/gr/sw_bundle_init.bin \
    /lib/firmware/nvidia/gm206/gr/sw_nonctx.bin \
    /lib/firmware/nvidia/gm206/gr/sw_ctx.bin \
    /lib/firmware/nvidia/gm206/gr/gpccs_sig.bin \
    /lib/firmware/nvidia/gm206/gr/gpccs_data.bin \
    /lib/firmware/nvidia/gm206/gr/gpccs_inst.bin \
    /lib/firmware/nvidia/gm206/gr/gpccs_bl.bin \
    /lib/firmware/nvidia/gm206/gr/fecs_sig.bin \
    /lib/firmware/nvidia/gm206/gr/fecs_data.bin \
    /lib/firmware/nvidia/gm206/gr/fecs_inst.bin \
    /lib/firmware/nvidia/gm206/gr/fecs_bl.bin \
    /lib/firmware/nvidia/gm206/acr/ucode_unload.bin \
    /lib/firmware/nvidia/gm206/acr/ucode_load.bin \
    /lib/firmware/nvidia/gm206/acr/bl.bin \
    /lib/firmware/nvidia/gm204/gr/sw_method_init.bin \
    /lib/firmware/nvidia/gm204/gr/sw_bundle_init.bin \
    /lib/firmware/nvidia/gm204/gr/sw_nonctx.bin \
    /lib/firmware/nvidia/gm204/gr/sw_ctx.bin \
    /lib/firmware/nvidia/gm204/gr/gpccs_sig.bin \
    /lib/firmware/nvidia/gm204/gr/gpccs_data.bin \
    /lib/firmware/nvidia/gm204/gr/gpccs_inst.bin \
    /lib/firmware/nvidia/gm204/gr/gpccs_bl.bin \
    /lib/firmware/nvidia/gm204/gr/fecs_sig.bin \
    /lib/firmware/nvidia/gm204/gr/fecs_data.bin \
    /lib/firmware/nvidia/gm204/gr/fecs_inst.bin \
    /lib/firmware/nvidia/gm204/gr/fecs_bl.bin \
    /lib/firmware/nvidia/gm204/acr/ucode_unload.bin \
    /lib/firmware/nvidia/gm204/acr/ucode_load.bin \
    /lib/firmware/nvidia/gm204/acr/bl.bin \
    /lib/firmware/nvidia/gm200/gr/sw_method_init.bin \
    /lib/firmware/nvidia/gm200/gr/sw_bundle_init.bin \
    /lib/firmware/nvidia/gm200/gr/sw_nonctx.bin \
    /lib/firmware/nvidia/gm200/gr/sw_ctx.bin \
    /lib/firmware/nvidia/gm200/gr/gpccs_sig.bin \
    /lib/firmware/nvidia/gm200/gr/gpccs_data.bin \
    /lib/firmware/nvidia/gm200/gr/gpccs_inst.bin \
    /lib/firmware/nvidia/gm200/gr/gpccs_bl.bin \
    /lib/firmware/nvidia/gm200/gr/fecs_sig.bin \
    /lib/firmware/nvidia/gm200/gr/fecs_data.bin \
    /lib/firmware/nvidia/gm200/gr/fecs_inst.bin \
    /lib/firmware/nvidia/gm200/gr/fecs_bl.bin \
    /lib/firmware/nvidia/gm200/acr/ucode_unload.bin \
    /lib/firmware/nvidia/gm200/acr/ucode_load.bin \
    /lib/firmware/nvidia/gm200/acr/bl.bin\
    /lib/firmware/nvidia/gm20b/gr/sw_method_init.bin\
    /lib/firmware/nvidia/gm20b/gr/sw_bundle_init.bin\
    /lib/firmware/nvidia/gm20b/gr/sw_nonctx.bin\
    /lib/firmware/nvidia/gm20b/gr/sw_ctx.bin \
    /lib/firmware/nvidia/gm20b/gr/gpccs_data.bin \
    /lib/firmware/nvidia/gm20b/gr/gpccs_inst.bin \
    /lib/firmware/nvidia/gm20b/gr/fecs_sig.bin \
    /lib/firmware/nvidia/gm20b/gr/fecs_data.bin \
    /lib/firmware/nvidia/gm20b/gr/fecs_inst.bin \
    /lib/firmware/nvidia/gm20b/gr/fecs_bl.bin \
    /lib/firmware/nvidia/gm20b/acr/ucode_load.bin \
    /lib/firmware/nvidia/gm20b/acr/bl.bin \
    "

FILES_${PN}-ath9k-htc = " \
    /lib/firmware/ath9k_htc/htc_9271-1.4.0.fw \
    /lib/firmware/ath9k_htc/htc_7010-1.4.0.fw \
    "
    
