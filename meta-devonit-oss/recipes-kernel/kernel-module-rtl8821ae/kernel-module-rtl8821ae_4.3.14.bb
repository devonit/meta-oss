SUMMARY = "RTL 8821ae Wifi Driver 4.3.14"
LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://${COREBASE}/meta/files/common-licenses/Proprietary;md5=0557f9d92cf58f2ccdd50f62f8ac0b28"

SRC_URI[md5sum] = "c8e5d2d077cd98d7a5da66acb304f1a4"
SRC_URI[sha256sum] = "6efeaa7c877f21c2584534775697e424d6c07cb85fe581b10ad614d87a69dd4a"

inherit module

DEVON_MIRRORS ?= "http://downloads.devonit.com/Development/accounts/development/sources/"
RTL_PKG = "rtl8821AE_linux_v4.3.14_15925.20151126_BTCOEX20150128-51"
RTL_PKG_NAME = "${RTL_PKG}_kernel_4.3.tar.gz"
PR = "r15925"

SRC_URI = "${DEVON_MIRRORS}/vendor/realtek/${RTL_PKG_NAME} \
           file://rtl8821ae-module-makefile.patch \
  "

S = "${WORKDIR}/${RTL_PKG}"

export USER_EXTRA_CFLAGS = "-Wno-error=date-time"

do_install () {
  oe_runmake -C "${STAGING_KERNEL_DIR}" M="${S}" DEPMOD=echo INSTALL_MOD_PATH="${D}" modules_install
}
