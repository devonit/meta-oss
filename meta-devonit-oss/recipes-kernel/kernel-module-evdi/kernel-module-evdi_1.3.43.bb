SUMMARY = "DisplayLink evdi driver 1.1.65"
LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/GPL-2.0;md5=801f80980d171dd6425610833a22dbe6"

DEPENDS = "virtual/kernel libdrm"

inherit module

PR = "r1"

S = "${WORKDIR}/git"

SRC_URI = " \
    git://github.com/DisplayLink/evdi.git;protocol=https;tag=v${PV};nobranch=1\
"
CFLAGS += "-I../module"
do_compile () {
    pushd module
    oe_runmake KVER="${KERNEL_VERSION}" \
               KDIR="${STAGING_KERNEL_BUILDDIR}" \
               M="${S}" module 
    popd
}

do_install () {
    install -d ${D}/lib/modules/${KERNEL_VERSION}/extra
    install -m644 ${S}/module/evdi.ko ${D}/lib/modules/${KERNEL_VERSION}/extra/evdi.ko

    install -d ${D}${libdir}/displaylink
}

FILES_${PN} = "\
    /lib/modules/${KERNEL_VERSION}/extra/evdi.ko \
"

FILES_${PN}-dbg = " ${libdir}/displaylink/.debug "
