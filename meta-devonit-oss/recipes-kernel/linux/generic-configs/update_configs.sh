#!/bin/sh

git clone git://git.yoctoproject.org/yocto-kernel-cache

for F in $(find yocto-kernel-cache -name "*.cfg")
do
    mv $F .
done

rm -rf yocto-kernel-cache

