DESCRIPTION = "Linux Kernel"
SECTION = "kernel"
LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://COPYING;md5=d7810fab7487fb0aad327b76f1be7cd7"
FILESEXTRAPATHS_prepend := "${THISDIR}/${PN}:${THISDIR}/${PN}-${PV}:"

LOCALVERSION ?= ""

SRC_URI = " \
            git://git.kernel.org/pub/scm/linux/kernel/git/stable/linux-stable.git;protocol=git;nocheckout=1;name=kernel;branch=${KBRANCH} \
          "
LINUX_VERSION = "${PV}"
LINUX_KERNEL_TYPE = "standard"
KMETA = "kernel-meta"
LINUX_VERSION_EXTENSION = "-devonit-${LINUX_KERNEL_TYPE}"


inherit kernel
require recipes-kernel/linux/linux-yocto.inc
require recipes-kernel/linux/aufs4-kernel-patches.inc
require recipes-kernel/linux/kernel-configs.inc

# $ pngtopnm -plain mylogo.png > mylogo.pnm
# $ ppmquant -plain 224 mylogo.pnm > logo_linux_clut224.ppm
# SRC_URI += "file://logo_linux_clut224.ppm"

# Set the verbosity of kernel messages during runtime
# You can define CMDLINE_DEBUG in your local.conf or distro.conf to override this behaviour
CMDLINE_DEBUG ?= "loglevel=3"

#kernel_conf_variable CMDLINE "\"${CMDLINE} ${CMDLINE_DEBUG}\""
kernel_conf_variable() {
    CONF_SED_SCRIPT="$CONF_SED_SCRIPT /CONFIG_$1[ =]/d;"
    if test "$2" = "n"
    then
        echo "# CONFIG_$1 is not set" >> '${WORKDIR}/defconfig'
    else
        echo "CONFIG_$1=$2" >> '${WORKDIR}/defconfig'
    fi
}

do_configure_prepend() {
    CONF_SED_SCRIPT=""

    if [ -e ${WORKDIR}/logo_linux_clut224.ppm ]; then
        install -m 0644 ${WORKDIR}/logo_linux_clut224.ppm ${S}/drivers/video/logo/logo_linux_clut224.ppm
        kernel_conf_variable LOGO y
        kernel_conf_variable LOGO_LINUX_CLUT224 y
    fi

    kernel_conf_variable CMDLINE_BOOL y
    kernel_conf_variable CMDLINE "\"${CMDLINE} ${CMDLINE_DEBUG}\""

    sed -i "${CONF_SED_SCRIPT}" '${B}/.config'
    cat '${WORKDIR}/defconfig' >>'${B}/.config'

}

do_install_append() {
    oe_runmake headers_install INSTALL_HDR_PATH=${D}${exec_prefix}/src/linux-${KERNEL_VERSION} ARCH=$ARCH
}

PACKAGES =+ "kernel-headers"
FILES_kernel-headers = "${exec_prefix}/src/linux*"

