require recipes-kernel/linux/linux-devonit-4.inc

PR = "r1"

KBRANCH = "linux-4.4.y"
SRCREV_kernel = "v${PV}" 
SRCREV_meta = "698835841165b68089604398f68fd8bc3f79cb65"
SRC_URI += "git://git.yoctoproject.org/yocto-kernel-cache;type=kmeta;name=meta;branch=yocto-4.4;destsuffix=${KMETA}"

