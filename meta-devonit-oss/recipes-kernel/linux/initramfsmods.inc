PACKAGES =+ "kernel-initramfs-modules kernel-reflash-initramfs-modules"

INC_PR .= "3"

ALLOW_EMPTY_kernel-initramfs-modules = "1"
ALLOW_EMPTY_kernel-reflash-initramfs-modules = "1"

EXTRA_PACKAGES = " \
                   ${@base_conditional('AUFS_FS_MODE', 'm', 'kernel-module-aufs', '',d)} \
                 "

INITRAMFS_BASE_MODULES ?= ""

INITRAMFS_MODDIRS_EXCLUDES ?= " \
                                net/appletalk \
                                net/arcnet \
                                net/bonding \
                                net/can \
                                net/hamradio \
                                net/irda \
                                net/pcmcia \
                                net/tokenring \
                                net/usb \
                                net/wan \
                                net/wimax \
                                net/wireless \
                                net/fddi \
                                net/ceph \
                                net/caif \
                                block/paride \
                            "
INITRAMFS_MODDIRS ?= " "


REFLASH_INITRAMFS_BASE_MODULES ?= "${INITRAMFS_BASE_MODULES}"
REFLASH_INITRAMFS_MODDIRS_EXCLUDES ?= "${INITRAMFS_MODDIRS_EXCLUDES}"
REFLASH_INITRAMFS_MODDIRS ?= "${INITRAMFS_MODDIRS}"

populate_packages_prepend () {
    import re,os,fnmatch,subprocess
    def get_ko_files( rootdir, ignorelist):
        result_ko_files = []
        for root, dirnames, filenames in os.walk(rootdir):
            for filename in fnmatch.filter(filenames, '*.ko'):
                if not any(item in root for item in ignorelist):
                    result_ko_files.append(os.path.join(root, filename))
        return result_ko_files

    def base_module_exists( rootdir, base_module ):
        result_ko_files = []
        for root, dirnames, filenames in os.walk(rootdir):
            filename = '%s.ko' % base_module
            if filename in fnmatch.filter(filenames, filename):
                return True
        return False

    def get_firmware( module ):
        root_dir = '%s/lib' % bb.data.getVar('PKGD',d, 1)
        result_firmwares = []
        modinfo = subprocess.Popen(['modinfo', '-F', 'firmware', module], stdout=subprocess.PIPE)
        stdout_value,stderr_value = modinfo.communicate()
        firmwares = stdout_value.split()

        for firmware in firmwares:
            if os.path.isfile('%s/firmware/%s' % (root_dir,firmware) ):
                result_firmwares.append(firmware)

        return result_firmwares

    def resolve_packages( base_modules, rootdir, ko_dirs, ignore_list ):
        ko_files = []
        kernel_modules = []
        kernel_firmwares = []

        for module in ko_dirs:
            ko_files.extend( get_ko_files( '%s/%s' % (rootdir, module), ignore_list) )

        for module in base_modules:
            if base_module_exists(rootdir, module):
                kernel_modules.append( legitimize_package_name( 'kernel-module-%s' % module))
            else:
                bb.fatal('Module %s not found.  You need to remove this module from INITRAMFS_BASE_MODULES or modify your kernel config to compile it as a module.' % module)

        ko_re = re.compile(r'^(.*)\.ko$')
        bin_re = re.compile(r'^(.*)\.bin$')
        fw_re = re.compile(r'^(.*)\.fw$')
        for ko_file in ko_files:
            kernel_modules.append( legitimize_package_name( 'kernel-module-%s' % os.path.basename(ko_re.match(ko_file).group(1))))

            for firmware_file in get_firmware(ko_file):
                if bin_re.match(firmware_file):
                    kernel_firmwares.append(legitimize_package_name( 'kernel-firmware-%s' % os.path.basename(bin_re.match(firmware_file).group(1))))
                if fw_re.match(firmware_file):
                    kernel_firmwares.append(legitimize_package_name( 'kernel-firmware-%s' % os.path.basename(fw_re.match(firmware_file).group(1))))
        return (list(set(kernel_modules)),list(set(kernel_firmwares)))

    ko_dirs = bb.data.getVar('INITRAMFS_MODDIRS',d, 1).split()
    ignore_list = bb.data.getVar('INITRAMFS_MODDIRS_EXCLUDES',d, 1).split()
    base_modules = bb.data.getVar('INITRAMFS_BASE_MODULES',d, 1).split()
    root = '%s/lib/modules/%s' % ( bb.data.getVar('PKGD',d, 1), bb.data.getVar('KERNEL_VERSION',d,1))
    extra_packages = bb.data.getVar('EXTRA_PACKAGES',d, 1).split()

    initramfs_modules,initramfs_firmwares = resolve_packages( base_modules, root, ko_dirs, ignore_list )
    initramfs_modules.extend( extra_packages )

    bb.data.setVar('RDEPENDS_kernel-initramfs-modules', ' '.join(initramfs_modules), d)
    bb.data.setVar('RRECOMMENDS_kernel-initramfs-modules', ' '.join(initramfs_firmwares), d)


    #Add any extra modules needed to downgrade to 7.2 in a seperate package
    reflash_ko_dirs = bb.data.getVar('REFLASH_INITRAMFS_MODDIRS',d, 1).split()
    reflash_ignore_list = bb.data.getVar('REFLASH_INITRAMFS_MODDIRS_EXCLUDES',d, 1).split()
    reflash_base_modules = bb.data.getVar('REFLASH_INITRAMFS_BASE_MODULES',d, 1).split()

    reflash_initramfs_modules,reflash_initramfs_firmwares = resolve_packages( reflash_base_modules, root, reflash_ko_dirs, reflash_ignore_list )
    reflash_initramfs_modules.extend( extra_packages )

    bb.data.setVar('RDEPENDS_kernel-reflash-initramfs-modules', ' '.join(reflash_initramfs_modules), d)
    bb.data.setVar('RRECOMMENDS_kernel-reflash-initramfs-modules', ' '.join(reflash_initramfs_firmwares), d)
}
