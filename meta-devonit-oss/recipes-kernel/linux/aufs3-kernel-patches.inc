AUFS_FS_MODE ?= "m"

python __anonymous () {
    aufs_srcrevs = {
        'aufs3.19':'cb95a08bdd37434ca8ba3a92679a5f33c48d7524',
        'aufs3.18.1+':'620e9d9fa72c51c2fb035bad17d7e38d9aed3fe9',
        'aufs3.18':'6c0a4e78594c9621e2142eb38bb63436aef3f1e4',
        'aufs3.13':'5bc28eeb80b3ea7bf377f15ffd4b07863160e665',
        'aufs3.12':'b9310bee5c8df6c1cbe73cfc47f6262ba4316885',
        'aufs3.11':'38f154e03d5c803d1c234511a4b9da1aeaa7f015',
        'aufs3.10':'63f4c9ebe476615863e23ccaf916df094b0525fd',
        'aufs3.9':'9fba96903fdcb7b1062f6ad04396ef192ff23a8c',
        'aufs3.8':'e98c69e26250b411e51cc92bf73df2f0829d0759',
        'aufs3.7':'27b5f7469fe5259aa489e92fdb6d88900ec8c0a4',
        'aufs3.6':'82d56105d0bdbdf5959b16f788fed4f6a530373f',
        'aufs3.5':'3e310a136e71bb284a959d95c77f5b7af132280b',
        'aufs3.4':'bfbe10165cbfc0cd7b1d7e9c878f1a3f2b6872f1',
        'aufs3.3':'df60b373c5f6c22835fdb8521b12973e9d6e06df',
        'aufs3.2':'5809bf47aeb6e8257691287f9a218660c110acc5',
        'aufs3.1':'269a613efab1718fd587c2bfc945d095b57f56e2',
        'aufs3.0':'aa3d7447003abd5e3c437de52d8da2e6203390ac'
    }
    aufs_ver = d.getVar('AUFS_VERSION', True)
    if aufs_ver:
        if aufs_ver in aufs_srcrevs:
            d.setVar('AUFS_REV', aufs_srcrevs[aufs_ver])
        else:
            bb.fatal('Not a supported AUFS_REV')
}

SRC_URI_append = " git://git.code.sf.net/p/aufs/aufs3-standalone;branch=${AUFS_VERSION};protocol=git;destsuffix=aufs;name=aufs;rev=${AUFS_REV} "

do_setup_additional_sources() {

  pushd ${S}
    patch -p1  < ${WORKDIR}/aufs/aufs3-base.patch
    if [ -f ${WORKDIR}/aufs/aufs3-mmap.patch ]; then
        patch -p1  < ${WORKDIR}/aufs/aufs3-mmap.patch
    fi
    if [ -f ${WORKDIR}/aufs/aufs3-proc_map.patch ]; then
        patch -p1  < ${WORKDIR}/aufs/aufs3-proc_map.patch
    fi
    patch -p1  < ${WORKDIR}/aufs/aufs3-kbuild.patch
    patch -p1  < ${WORKDIR}/aufs/aufs3-standalone.patch
  popd

  cp -r ${WORKDIR}/aufs/Documentation ${S}
  cp -r ${WORKDIR}/aufs/fs ${S}
  if [ -f ${WORKDIR}/aufs/include/linux/aufs_type.h ]; then
      cp ${WORKDIR}/aufs/include/linux/aufs_type.h ${S}/include/linux/
  fi
  if [ -f ${WORKDIR}/aufs/include/uapi/linux/aufs_type.h ]; then
      cp ${WORKDIR}/aufs/include/uapi/linux/aufs_type.h ${S}/include/uapi/linux/
  fi

  echo "${@base_conditional('AUFS_FS_MODE', 'm', 'CONFIG_AUFS_FS=m', '',d)}" >> ${WORKDIR}/defconfig
  echo "${@base_conditional('AUFS_FS_MODE', 'y', 'CONFIG_AUFS_FS=y', '',d)}" >> ${WORKDIR}/defconfig
  echo "CONFIG_AUFS_BRANCH_MAX_511=y" >> ${WORKDIR}/defconfig
  echo "CONFIG_AUFS_BR_FUSE=y" >> ${WORKDIR}/defconfig
  echo "CONFIG_AUFS_BR_RAMFS=y" >> ${WORKDIR}/defconfig
  echo "CONFIG_AUFS_BDEV_LOOP=y" >> ${WORKDIR}/defconfig
  echo "CONFIG_AUFS_POLL=y" >> ${WORKDIR}/defconfig
  echo "CONFIG_AUFS_SBILIST=y" >> ${WORKDIR}/defconfig 
  echo "CONFIG_AUFS_EXPORT=y" >> ${WORKDIR}/defconfig
  echo "CONFIG_AUFS_PROC_MAP=y" >> ${WORKDIR}/defconfig
  echo "CONFIG_AUFS_SHWH=y" >> ${WORKDIR}/defconfig
}


addtask setup_additional_sources after do_patch before do_configure
