FILESEXTRAPATHS_prepend := "${THISDIR}/${PN}:"

LOGO_SIZE = "vga"

DEPENDS += "bc-native"

SRC_URI += " \
    ${KERNELORG_MIRROR}/linux/kernel/v3.x/linux-${PV}.${PATCH_LVL}.tar.xz;name=kernel \
    file://defconfig \
"

inherit kernel
require recipes-kernel/linux/aufs3-kernel-patches.inc
require recipes-kernel/linux/initramfsmods.inc

S = "${WORKDIR}/linux-${PV}.${PATCH_LVL}"

EXTRA_OEMAKE="${PARALLEL_MAKE}"
