AUFS_FS_MODE ?= "m"
AUFS_VERSION ?= "aufs4.0"

def get_aufs_rev(d):
    aufs_srcrevs = {
        'aufs4.0':'27126132c17d66c1ab6b62649ce40511d2129b69',
        'aufs4.1':'172ebcd594ef501cb661cbb5e6f1626f93b25118',
        'aufs4.1.13+':'807b4972e411752347948698c8de99a9b4660842',
        'aufs4.2':'7832b7e14454d4a8850c904995bb84e517d5fffc',
        'aufs4.3':'b4600d2a74fb6efbc7577005bd236debdaddfa97',
        'aufs4.4':'ab2083cbed8619eccef23941c2c0c83357af7199',
        'aufs4.5':'31f6b73e4b74ec3b45bf32ce075405ee483fd7b9',
        'aufs4.6':'244642729f45fd79cb3b91874ff0c82b4cfba328',
        'aufs4.9':'ecd2bfe6f46f36658c9aef74fc43ac40e47f8438',
    }
    aufs_ver = d.getVar('AUFS_VERSION', True)
    if aufs_ver:
        if aufs_ver in aufs_srcrevs:
            return aufs_srcrevs[aufs_ver]
        else:
            bb.fatal('Not a supported AUFS_VERSION')
    return ''

SRCREV_aufs = "${@get_aufs_rev(d)}"
SRC_URI_append = " git://github.com/sfjro/aufs4-standalone.git;branch=${AUFS_VERSION};protocol=git;destsuffix=aufs;name=aufs "

do_setup_additional_sources() {

  cd ${S}
  patch -p1  < ${WORKDIR}/aufs/aufs4-base.patch
  if [ -f ${WORKDIR}/aufs/aufs4-mmap.patch ]; then
      patch -p1  < ${WORKDIR}/aufs/aufs4-mmap.patch
  fi
  if [ -f ${WORKDIR}/aufs/aufs4-proc_map.patch ]; then
      patch -p1  < ${WORKDIR}/aufs/aufs4-proc_map.patch
  fi
  patch -p1  < ${WORKDIR}/aufs/aufs4-kbuild.patch
  patch -p1  < ${WORKDIR}/aufs/aufs4-standalone.patch

  cp -r ${WORKDIR}/aufs/Documentation ${S}
  cp -r ${WORKDIR}/aufs/fs ${S}
  if [ -f ${WORKDIR}/aufs/include/linux/aufs_type.h ]; then
      cp ${WORKDIR}/aufs/include/linux/aufs_type.h ${S}/include/linux/
  fi
  if [ -f ${WORKDIR}/aufs/include/uapi/linux/aufs_type.h ]; then
      cp ${WORKDIR}/aufs/include/uapi/linux/aufs_type.h ${S}/include/uapi/linux/
  fi

  echo "${@base_conditional('AUFS_FS_MODE', 'm', 'CONFIG_AUFS_FS=m', '',d)}" >> ${WORKDIR}/defconfig
  echo "${@base_conditional('AUFS_FS_MODE', 'y', 'CONFIG_AUFS_FS=y', '',d)}" >> ${WORKDIR}/defconfig
  echo "CONFIG_AUFS_BRANCH_MAX_511=y" >> ${WORKDIR}/defconfig
  echo "CONFIG_AUFS_BR_FUSE=y" >> ${WORKDIR}/defconfig
  echo "CONFIG_AUFS_BR_RAMFS=y" >> ${WORKDIR}/defconfig
  echo "CONFIG_AUFS_BDEV_LOOP=y" >> ${WORKDIR}/defconfig
  echo "CONFIG_AUFS_POLL=y" >> ${WORKDIR}/defconfig
  echo "CONFIG_AUFS_SBILIST=y" >> ${WORKDIR}/defconfig 
  echo "CONFIG_AUFS_EXPORT=y" >> ${WORKDIR}/defconfig
  echo "CONFIG_AUFS_PROC_MAP=y" >> ${WORKDIR}/defconfig
  echo "CONFIG_AUFS_SHWH=y" >> ${WORKDIR}/defconfig
}

addtask setup_additional_sources after do_patch before do_configure

