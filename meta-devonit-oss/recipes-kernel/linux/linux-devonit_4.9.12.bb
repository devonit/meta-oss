require recipes-kernel/linux/linux-devonit-4.inc

PR = "r1"

KBRANCH = "linux-4.9.y"
SRCREV_kernel = "v${PV}" 
SRCREV_meta = "6fd9dcbb3f0becf90c555a1740d21d18c331af99"
SRC_URI += "git://git.yoctoproject.org/yocto-kernel-cache;type=kmeta;name=meta;branch=yocto-4.9;destsuffix=${KMETA}"

