DESCRIPTION = "DevonIT kernel"
SECTION = "kernel"
LICENSE = "GPLv2"

# Skip processing of this recipe if it is not explicitly specified as the
# PREFERRED_PROVIDER for virtual/kernel. This avoids network access required
# by the use of AUTOREV SRCREVs, which are the default for this recipe.
python () {
    if d.getVar("PREFERRED_VERSION_linux-devonit", True) != "3.18": 
        raise bb.parse.SkipPackage("Set PREFERRED_VERSION_linux-devonit to 3.18 to enable");
}

require recipes-kernel/linux/linux-devonit.inc

PATCH_LVL = "26"

FILESEXTRAPATHS_prepend := "${THISDIR}/${PN}-${PV}:"

# Every time defconfig changes add one to PR
PR = "r1.${INC_PR}"

SRC_URI += " \
    file://0001-xhci-Switch-only-Intel-Lynx-Point-LP.patch \
"

SRC_URI[kernel.md5sum] = "952c9159acdf4efbc96e08a27109d994"
SRC_URI[kernel.sha256sum] = "0d795f28cba56ba7fb1ccb4552dd7b0804957e23c9cab14b92166b1a5f4f95d7"

