require recipes-kernel/linux/linux-devonit-4.inc

PR = "r1"

KBRANCH = "linux-4.9.y"
SRCREV_kernel = "v${PV}" 
SRCREV_meta = "0774eacea2a7d3a150594533b8c80d0c0bfdfded"
SRC_URI += "git://git.yoctoproject.org/yocto-kernel-cache;type=kmeta;name=meta;branch=yocto-4.9;destsuffix=${KMETA}"

