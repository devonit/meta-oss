require recipes-kernel/linux/linux-devonit-4.inc

PR = "r1"
KBRANCH = "linux-4.1.y"
SRCREV_kernel = "69fb67fa753ea5af658b5a696f091edffd704260"
SRCREV_meta = "eef309262d79f1f391a8a6a9a53eb49d817a5fa1"
SRC_URI += "git://git.yoctoproject.org/yocto-kernel-cache;type=kmeta;name=meta;branch=yocto-4.1;destsuffix=${KMETA}"

