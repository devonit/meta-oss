require presage.inc
DEPENDS += "sqlite3"
PR = "${INC_PR}.0"
LIC_FILES_CHKSUM = "file://COPYING;md5=94d55d512a9ba36caa9b7df079bae19f"

SRCREV = "09c03eeff799f83de047f800d4392afc667bf2bd"

S = "${WORKDIR}/git"

do_run_bootstrap () {
    pushd ${S}
    ./bootstrap
    popd
}

addtask run_bootstrap before do_configure after do_patch

do_run_bootstrap[depends] += "autoconf-native:do_populate_sysroot \
                              automake-native:do_populate_sysroot \
                              libtool-native:do_populate_sysroot "


FILES_${PN} = ""
FILES_${PN}-dev = ""
FILES_${PN}-dbg = ""
