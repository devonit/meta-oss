DESCRIPTION = "An intelligent predictive text entry system."
HOMEPAGE = "http://presage.sourceforge.net/"
LICENSE = "GPLv2"
INC_PR = "r0"

SRC_URI = "git://git.code.sf.net/p/presage/presage;protocol=http"

DEPENDS = "automake automake-native autoconf autoconf-native"
