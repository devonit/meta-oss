require anthy_${PV}.inc

DEPENDS = ""
PACKAGES = ""
PR = "r1"

S = "${WORKDIR}/anthy-${PV}"
SRC_URI += "file://install-helpers-to-sysroot.patch"

inherit native
