DESCRIPTION = "Telnet client with SSL encryption support"
LICENSE = "BSD&GPLv2"
LIC_FILES_CHKSUM = "file://debian/copyright;md5=71d470273474b2f9290758c8265cd40a"

inherit autotools
inherit useradd

PR = "r0"
DEBIAN_REV = "24"

S = "${WORKDIR}/${PN}-${PV}.orig"
B = "${S}"
PARALLEL_MAKE = ""

DEPENDS += " \
    ncurses \
    openssl \
    "

SRC_URI = " \
    http://http.debian.net/debian/pool/main/n/${PN}/${PN}_${PV}.orig.tar.gz;name=src \
    http://http.debian.net/debian/pool/main/n/${PN}/${PN}_${PV}-${DEBIAN_REV}.diff.gz;name=patch \
    "

EXTRA_OEMAKE = " \
    INSTALLROOT=${D} \
    SBINDIR=${sbindir} \
    DAEMONMODE=755 \
    MANMODE=644 \
    MANDIR=${mandir} \
    CC='${CC}' \
    LD='${LD}' \
    LDFLAGS='${LDFLAGS}' \
    "

CONFIGUREOPTS = " \
    --prefix=${prefix} \
    "

do_configure_append () {
    sed -e 's#^CFLAGS=\(.*\)$#CFLAGS= -D_GNU_SOURCE -I${STAGING_DIR_TARGET}/usr/include/openssl \1#' \
        -e 's#^CXXFLAGS=\(.*\)$#CXXFLAGS= -D_GNU_SOURCE -I${STAGING_DIR_TARGET}/usr/include/openssl \1#' \
        -e 's#^LDFLAGS=.*$#LDFLAGS= ${LDFLAGS}#' \
        -i MCONFIG
}

do_install () {
    install -d ${D}${bindir}
    install -d ${D}${sbindir}
    install -d ${D}${mandir}/man1
    install -d ${D}${mandir}/man5
    install -d ${D}${mandir}/man8
    oe_runmake install SUB=telnet
    oe_runmake install SUB=telnetd
    oe_runmake install SUB=telnetlogin
}

PACKAGES = " \
    telnet-ssl \
    telnet-ssl-doc \
    telnet-ssl-dbg \
    telnet-ssl-dev \
    telnetd-ssl \
    telnetd-ssl-doc \
    telnetd-ssl-dbg \
    "

FILES_telnet-ssl += " \
    ${bindir}/telnet-ssl \
    "

FILES_telnet-ssl-doc += " \
    ${mandir}/man1/telnet-ssl.1 \
    "

FILES_telnet-ssl-dbg += " \
    ${bindir}/.debug/telnet-ssl \
    "

FILES_telnet-ssl-dev += " \
    /usr/src \
    "

FILES_telnetd-ssl += " \
    ${sbindir}/in.telnetd \
    ${sbindir}/telnetlogin \
    "

FILES_telnetd-ssl-doc += " \
    ${mandir}/man5/ \
    ${mandir}/man8/ \
    "

FILES_telnetd-ssl-dbg += " \
    ${sbindir}/.debug/in.telnetd \
    ${sbindir}/.debug/telnetlogin \
    "

USERADD_PACKAGES = "telnetd-ssl"
GROUPADD_PARAM_telnetd-ssl = "telnetd"

SRC_URI[src.md5sum] = "43a402139ed6b86434fdb83256feaad8"
SRC_URI[src.sha256sum] = "1a4b45826b37bea4d8e1ac3e3ae8f7727ce87efd239038a439c80ad52c33bed7"

SRC_URI[patch.md5sum] = "c557bd9deb0e65e39ba1627ae44c13cb"
SRC_URI[patch.sha256sum] = "8c92c95d32972296b5beb8ac59cf23b1a030d49628d4675d2e46873306e2e468"

