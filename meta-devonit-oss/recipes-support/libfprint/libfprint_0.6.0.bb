DESCRIPTION = "Async fingerprint library"
LICENSE = "LGPLv2.1"
LIC_FILES_CHKSUM = "file://COPYING;md5=fbc093901857fcd118f065f900982c24"

DEPENDS += " \
    libusb \
    nss \
"

inherit autotools pkgconfig

PR = "r1"

SRC_URI = " \
    https://people.freedesktop.org/~hadess/libfprint-${PV}.tar.xz \
"

S = "${WORKDIR}/libfprint-${PV}"

EXTRA_OECONF += "--disable-udev-rules --with-drivers=all"

SRC_URI[md5sum] = "1e66f6e786348b46075368cc682450a8"
SRC_URI[sha256sum] = "2583fcb7d542a918c023776f188067fcedec614e65494dd52bc4d661be803cbe"

