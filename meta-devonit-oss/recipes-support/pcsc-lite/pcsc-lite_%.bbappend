FILESEXTRAPATHS_prepend := "${THISDIR}/${PN}:"

SRC_URI = " \
             https://alioth.debian.org/frs/download.php/file/3757/pcsc-lite-${PV}.tar.bz2 \
             file://pcscd.service \
          "

do_install_append() {
   install ${WORKDIR}/pcscd.service ${D}${systemd_unitdir}/system/pcscd.service
}

FILES_${PN}-lib =+ "${libdir}/libpcsclite.so"
INSANE_SKIP_${PN}-lib += "dev-so"
