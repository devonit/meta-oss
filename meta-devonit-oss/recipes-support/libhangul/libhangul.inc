DESCRIPTION = "A Korean input method"
HOMEPAGE = "https://code.google.com/p/libhangul/"
SECTION = "inputmethods"
LICENSE = "LGPLv2.1"
LIC_FILES_CHKSUM = "file://COPYING;md5=a6f89e2100d9b6cdffcea4f398e37343"
INC_PR = "r0"

inherit autotools pkgconfig gettext

SRC_URI = "https://libhangul.googlecode.com/files/${PN}-${PV}.tar.gz"

