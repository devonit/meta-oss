DESCRIPTION = "Rdesktop rdp client for X"
HOMEPAGE = "http://www.rdesktop.org"
SECTION = "x11/network"
LICENSE = "GPLv3"
LIC_FILES_CHKSUM = "file://COPYING;md5=f27defe1e96c2e1ecd4e0c9be8967949"
PR = "r2"

SRC_URI = "git://github.com/rdesktop/rdesktop.git;protocol=https;tag=v${PV};nobranch=1"

inherit gitpkgv autotools pkgconfig

B = "${S}"
S = "${WORKDIR}/git"

DEPENDS = " \
    virtual/libx11 \
    openssl \
    xrandr \
    pcsc-lite \
    alsa-lib \
    "

RDEPENDS_${PN} += "glibc-gconv-utf-16"


EXTRA_OECONF = " \
    --with-openssl=${STAGING_EXECPREFIXDIR} \
    --disable-smartcard \
    --disable-credssp \
    --with-sound=alsa \
    "
