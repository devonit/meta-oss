DESCRIPTION = "IBM 3270 emulators"
LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://LICENSE;md5=fdc7a1e76225aa3a7a926e12d7275a11"

inherit autotools

PR = "r0"

S = "${WORKDIR}/c3270-3.3"
B = "${S}"

DEPENDS += " \
    ncurses \
    openssl \
    "

EXTRA_OECONF = " \
    --enable-ansi \
    --enable-apl \
    --enable-dbcs \
    --enable-ft \
    --enable-local-process \
    --enable-printer \
    --enable-script \
    --enable-tn3270e \
    --enable-trace \
    --enable-ssl \
    --without-readline \
    --with-curses-wide \
    "

SRC_URI = " \
    ${SOURCEFORGE_MIRROR}/project/x3270/x3270/${PV}/suite3270-${PV}-src.tgz \
    "

SRC_URI[md5sum] = "13d904e46cf7cea1dd9b0cdb5d1a3ebd"
SRC_URI[sha256sum] = "56bfc90d8340b0d3c0f0ceda8851e27bde8ad09542620e7a583554745f4a8e66"

do_configure() {
    oe_runconf
}
