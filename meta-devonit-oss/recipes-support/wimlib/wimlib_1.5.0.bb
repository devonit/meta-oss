DESCRIPTION = "wimlib is a C library for creating, modifying, extracting, and mounting files in the Windows Imaging Format"
LICENSE = "GPLv3"
LIC_FILES_CHKSUM = "file://COPYING;md5=d32239bcb673463ab874e80d47fae504"
PR = "r1"

inherit pkgconfig autotools

DEPENDS += "openssl fuse libxml2 attr ntfs-3g-ntfsprogs"

SRC_URI = " \
            git://git.code.sf.net/p/wimlib/code.git \
          "
S = "${WORKDIR}/git"

SRCREV = "v${PV}"
EXTRA_OECONF += "--with-imagex-progname=imagex"

PACKAGES =+ "${PN}-imagex ${PN}-mkwinpeimg"
FILES_${PN}-imagex = "${bindir}/wim* ${bindir}/imagex"
FILES_${PN}-mkwinpeimg = "${bindir}/mkwinpeimg"

