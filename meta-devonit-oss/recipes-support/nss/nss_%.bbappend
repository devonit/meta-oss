FILESEXTRAPATHS_prepend := "${THISDIR}/files:"


SRC_URI += "file://nss.ld.so.conf"

do_install_append() {
    install -D ${WORKDIR}/nss.ld.so.conf ${D}/etc/ld.so.conf.d/nss
}

FILES_${PN} += "/etc/ld.so.conf.d/nss"
