DESCRIPTION = 'Encrypted filesystem in user-space'
HOMEPAGE ='https://vgough.github.io/encfs/'
LICENSE = "GPLv3"
LIC_FILES_CHKSUM = " \
    file://COPYING;md5=8b86b0ee5693ab67b30e022513686435 \
    file://COPYING.GPL;md5=d32239bcb673463ab874e80d47fae504 \
    FILE://COPYING.LGPL;md5=e6a600fd5e1d9cbde2d983680233ad02 \
    "

DEPENDS = " \
    openssl \
    fuse \ 
   "

SRC_URI[md5sum] = "7cbf9cc3c5af49b46703ce6ba70d22a4"
SRC_URI[sha256sum] = "67203aeff7a06ce7be83df4948db296be89a00cffe1108a0a41c96d7481106a4"

SRC_URI = " \
    https://github.com/vgough/${PN}/releases/download/v${PV}/${PN}-${PV}.tar.gz \
    "

inherit pkgconfig cmake
