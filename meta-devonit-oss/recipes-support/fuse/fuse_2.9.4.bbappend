SRC_URI = " \
    https://github.com/libfuse/libfuse/releases/download/fuse_2_9_4/fuse-${PV}.tar.gz \
    file://gold-unversioned-symbol.patch \
    file://aarch64.patch \
    file://0001-fuse-fix-the-return-value-of-help-option.patch \
    file://fuse.conf \
"

