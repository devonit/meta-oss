KDE_MIRROR = "http://download.kde.org"

SRC_URI = "${KDE_MIRROR}/stable/${BPN}/${PV}/src/${BPN}-${PV}.tar.xz"

# extra-cmake-modules cause dependent to check for python
inherit cmake pythonnative

do_compile_prepend() {
    export XDG_DATA_HOME=${STAGING_DATADIR}
}

FILES_${PN} += "${libdir}/plugins/kf5 ${datadir}/kf5 ${libexecdir}/kf5"
FILES_${PN}-dev += "${libdir}/cmake"
FILES_${PN}-dbg += "${libdir}/plugins/kf5/.debug ${libexecdir}/kf5/.debug"

EXTRA_OECMAKE += " \
    -DECM_MKSPECS_INSTALL_DIR=${OE_QMAKE_PATH_QT_ARCHDATA}/mkspecs/modules \
    -DCMAKE_INSTALL_DATADIR=share \
    -DCMAKE_INSTALL_DBUSINTERFACEDIR=share/dbus-1/interfaces \
    -DKDE_INSTALL_QTPLUGINDIR=${OE_QMAKE_PATH_PLUGINS} \
    -DKDE_INSTALL_QMLDIR=${OE_QMAKE_PATH_QML} \
"

FILES_${PN} += " \
    ${datadir}/appdata \
    ${datadir}/metainfo \
"

FILES_${PN}-dev += "${OE_QMAKE_PATH_QT_ARCHDATA}/mkspecs"

BBCLASSEXTEND = "native"

KF5_VERSION = "5.25.0"

SRC_URI = "${KDE_MIRROR}/stable/frameworks/5.25/${BPN}-${PV}.tar.xz"


SUMMARY = "Extra modules and scripts for CMake"
LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://COPYING-CMAKE-SCRIPTS;md5=54c7042be62e169199200bc6477f04d1"


SRC_URI[md5sum] = "043c08482bf7cf951e18d32e16238fb4"
SRC_URI[sha256sum] = "73c90e67c328076c2bee35884836087e7e9f753e48fc0063c8348b6f66b0345f"
#SRC_URI += "file://0001-FindQtWaylandScanner.cmake-align-path-to-our-needs.patch"

PV = "${KF5_VERSION}"

FILES_${PN} += "${datadir}/ECM"



