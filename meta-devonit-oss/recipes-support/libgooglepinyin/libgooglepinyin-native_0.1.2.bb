require libgooglepinyin.inc
inherit native
PR = "${INC_PR}.1"
LIC_FILES_CHKSUM = "file://INSTALL;md5=f66bac3139475499a00d2adbfbc655e3"

SRC_URI += "file://dictbuilder-native.patch \
            file://native-force-32-bit-build.patch \
           "

# The dictbuilder binary is required to build pinyin, but it is not installed
# Here, we install it manually so that the target recipe can find it
# We also force it to be a 32-bit executable, because 64-bit generates unusable files
do_install_dictbuilder () {
    install -d ${D}${base_prefix}/usr/bin
    install -m 755 ${OECMAKE_BUILDPATH}/tools/dictbuilder ${D}${base_prefix}/usr/bin
}

addtask install_dictbuilder before do_populate_sysroot after do_install

FILES_${PN} = "/usr/bin/dictbuilder"

SRC_URI[md5sum] = "d697aba08fdc0fe15c9d7b6096ca3b28"
SRC_URI[sha256sum] = "1a339ae45721a60b9fadd15e43c34b9bb27af3bb999c00ed0d88b4084cfd0637"
