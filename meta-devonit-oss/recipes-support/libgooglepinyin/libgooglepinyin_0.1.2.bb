require libgooglepinyin.inc
# native package required for dictbuilder
DEPENDS += "libgooglepinyin-native"
PR = "${INC_PR}.0"
LIC_FILES_CHKSUM = "file://INSTALL;md5=f66bac3139475499a00d2adbfbc655e3"

FILES_${PN} = "/usr/lib/libgooglepinyin \
               /usr/lib/libgooglepinyin.so.* \
               /usr/lib/googlepinyin/data/dict_pinyin.dat \
              "

FILES_${PN}-dev = "/usr/include/googlepinyin/* \
                   /usr/lib/pkgconfig \
                  "

FILES_${PN}-dbg = "/usr/src/debug/* \
                   /usr/lib/.debug \
                   /usr/lib/libgooglepinyin.so \
                  "

SRC_URI[md5sum] = "d697aba08fdc0fe15c9d7b6096ca3b28"
SRC_URI[sha256sum] = "1a339ae45721a60b9fadd15e43c34b9bb27af3bb999c00ed0d88b4084cfd0637"
