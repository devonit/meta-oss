DESCRIPTION = "An input method framework with extension support."
HOMEPAGE = "http://fcitx-im.org/"
SECTION = "inputmethods"
LICENSE = "GPLv2"
INC_PR = "r0"

inherit pkgconfig cmake

SRC_URI = "https://storage.googleapis.com/google-code-archive-downloads/v2/code.google.com/libgooglepinyin/libgooglepinyin-${PV}.tar.bz2"

S = "${WORKDIR}/libgooglepinyin-${PV}"
OECMAKE_SOURCEPATH = "${S}"
OECMAKE_BUILDPATH = "${WORKDIR}/build"

EXTRA_OECONF += "-C ${OECMAKE_BUILDPATH}"
