FILESEXTRAPATHS_prepend := "${THISDIR}/${PN}:"

# OVERRIDE to have openssl support (Citrix13)
DEPENDS = "zlib gnutls openssl"
CURLGNUTLS = " --with-gnutls=${STAGING_LIBDIR}/../"
EXTRA_OECONF += " --with-ssl=${STAGING_LIBDIR}/../ "

EXTRA_OECONF += " --with-ca-bundle=${sysconfdir}/ssl/certs/ca-certificates.crt"
RRECOMMENDS_lib${BPN} = "ca-certificates"
