DESCRIPTION = "Google Pinyin plugin for Fcitx."
require fcitx-plugin.inc
PR = "${INC_PR}.0"
LIC_FILES_CHKSUM = "file://COPYING;md5=d32239bcb673463ab874e80d47fae504"

DEPENDS += "libgooglepinyin"

SRC_URI[md5sum] = "0f45e0d744c93fa89a32aba05cc2aa6d"
SRC_URI[sha256sum] = "c041e93355015c56895dc6181d9e25ee84a57233d442f98f88078b1d1a416e0d"
