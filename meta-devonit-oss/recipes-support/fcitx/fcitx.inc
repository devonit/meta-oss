DESCRIPTION = "An input method framework with extension support."
HOMEPAGE = "http://fcitx-im.org/"
LICENSE = "GPLv2"
inherit pkgconfig cmake


INC_PR = "r0"

SRC_URI = "https://github.com/fcitx/fcitx/archive/${PV}.tar.gz"

S = "${WORKDIR}/fcitx-${PV}"
OECMAKE_SOURCEPATH = "${S}"
OECMAKE_BUILDPATH = "${WORKDIR}/build"

EXTRA_OECONF += "-C ${OECMAKE_BUILDPATH}"
