require fcitx.inc
inherit gettext systemd qt4x11

PR = "${INC_PR}.3"

DEPENDS = " \
    extra-cmake-modules fcitx-native libxrender libxfixes cairo pango dbus dbus-glib enchant \
    libxkbfile icu intltool-native iso-codes presage gtk+ libxinerama \
    "

RDEPENDS_${PN} = "glibc-charmap-euc-jp glibc-gconv-euc-jp"

LIC_FILES_CHKSUM = "file://COPYING;md5=6306e547e5c5e43190193019ed46ee13"

SRC_URI += " \
    file://fcitx_use-native-tools.patch \
    file://fcitx-init.sh \
    file://fcitx.profile \
    file://gtk-update-immodule-cache.sh \
    file://gtk-update-immodule-cache.service \
    file://fcitx-adjust-cmake-downloads.patch \
    "

SYSTEMD_PACKAGES += "${PN}"
SYSTEMD_SERVICE_${PN} = "gtk-update-immodule-cache.service"

EXTRA_OECMAKE += " \
    -DENABLE_GTK2_IM_MODULE=ON \
    -DENABLE_GTK3_IM_MODULE=OFF \
    -DENABLE_QT=ON \
    -DENABLE_QT_IM_MODULE=ON \
    -DENABLE_QT_GUI=ON \
    -DENABLE_CAIRO=ON \
    -DENABLE_PANGO=ON \
    -DENABLE_DBUS=ON \
    -DENABLE_DEBUG=OFF \
    -DENABLE_TABLE=ON \
    -DENABLE_STATIC=OFF \
    -DENABLE_TEST=OFF \
    -DENABLE_OPENCC=ON \
    -DENABLE_SNOOPER=ON \
    -DENABLE_GIR=OFF \
    -DENABLE_ENCHANT=ON \
    -DENABLE_PRESAGE=ON \
    "

EXTRA_OECMAKE += " \
    -DQT_LIBRARY_DIR=${OE_QMAKE_LIBDIR_QT} \
    -DQT_INSTALL_LIBS=${OE_QMAKE_LIBDIR_QT} \
    -DQT_PLUGINS_DIR=${libdir}/qt4/plugins/ \
    -DQT_INCLUDE_DIR=${OE_QMAKE_INCDIR_QT} \
    -DQT_HEADERS_DIR=${OE_QMAKE_INCDIR_QT} \
    -DQT_MOC_EXECUTABLE=${OE_QMAKE_MOC} \
    -DQT_UIC_EXECUTABLE=${OE_QMAKE_UIC} \
    -DQT_UIC3_EXECUTABLE=${OE_QMAKE_UIC3} \
    -DQT_RCC_EXECUTABLE=${OE_QMAKE_RCC} \
    -DQT_QMAKE_EXECUTABLE=${OE_QMAKE_QMAKE} \
    -DQT_QTCORE_INCLUDE_DIR=${OE_QMAKE_INCDIR_QT}/QtCore \
    -DQT_DBUSXML2CPP_EXECUTABLE=/usr/bin/qdbusxml2cpp \
    -DQT_DBUSCPP2XML_EXECUTABLE=/usr/bin/qdbuscpp2xml \
    -DQT_MKSPECS_DIR=${QMAKESPEC}/../ \
    -DQT_QTCORE_LIBRARY_RELEASE=${OE_QMAKE_LIBDIR_QT}/libQtCore.so \
    -DQT_QTDBUS_LIBRARY_RELEASE=${OE_QMAKE_LIBDIR_QT}/libQtDBus.so \
    -DQT_QTGUI_LIBRARY_RELEASE=${OE_QMAKE_LIBDIR_QT}/libQtGui.so \
    -DSYSCONFDIR=/etc/ \
    "

do_configure() {
    # Ensure we get the cmake configure and not qmake
    cmake_do_configure
}

do_install_append () {
    install -d ${D}${sysconfdir}/
    install -m 755 ${WORKDIR}/fcitx.profile ${D}${sysconfdir}/fcitx.profile

    install -d ${D}${sysconfdir}/X11/Xsession.d/
    install -m 755 ${WORKDIR}/fcitx-init.sh ${D}${sysconfdir}/X11/Xsession.d/05fcitx

    install -d ${D}${bindir}/
    install -d ${D}/{etc,lib}/systemd/system/

    install -m 0755 ${WORKDIR}/gtk-update-immodule-cache.sh ${D}${bindir}/gtk-update-immodule-cache.sh
    install -m 0755 ${WORKDIR}/gtk-update-immodule-cache.service ${D}/lib/systemd/system/gtk-update-immodule-cache.service
    install -d ${D}/etc/systemd/system/graphical.target.wants
    ln -s /lib/systemd/system/gtk-update-immodule-cache.service ${D}/etc/systemd/system/graphical.target.wants/gtk-update-immodule-cache.service
}

FILES_${PN} = " \
    ${bindir}/* \
    ${libdir}/* \
    ${datadir}/fcitx/* \
    ${datadir}/dbus-1/services/org.fcitx.Fcitx.service \
    ${datadir}/icons/hicolor/* \
    ${datadir}/mime/packages/x-fskin.xml \
    ${datadir}/applications/fcitx* \
    ${sysconfdir}/fcitx.profile \
    ${sysconfdir}/xdg/* \
    ${sysconfdir}/X11/Xsession.d/* \
    ${sysconfdir}/systemd/system/graphical.target.wants/gtk-update-immodule-cache.service \
    "

FILES_${PN}-dev = " \
    ${datadir}/cmake/fcitx/* \
    ${libdir}/*.so \
    ${includedir}/* \
    "

FILES_${PN}-dbg = " \
    /usr/src/debug/fcitx/* \
    ${bindir}/.debug/* \
    ${libdir}/.debug/* \
    ${libdir}/fcitx/.debug/* \
    ${libdir}/fcitx/libexec/.debug/* \
    ${libdir}/gtk-2.0/2.10.0/immodules/.debug/* \
    ${libdir}/fcitx/qt/.debug/* \
    ${libdir}/qt4/plugins/inputmethods/.debug/qtim-fcitx.so \
    "

SRC_URI[md5sum] = "8cffd2c001ad0636af75107e20d90fb5"
SRC_URI[sha256sum] = "f15a3acbef40fa1f05c38e99a2334fe437e5ee938332c4914c9736b6b57aeeda"

