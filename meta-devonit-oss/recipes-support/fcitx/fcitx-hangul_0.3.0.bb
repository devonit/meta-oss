DESCRIPTION = "Hangul plugin for Fcitx."
require fcitx-plugin.inc
PR = "${INC_PR}.0"
LIC_FILES_CHKSUM = "file://COPYING;md5=00ed06f01bcb1983382068710eb131c4"

DEPENDS += "libhangul"

SRC_URI[md5sum] = "3bc518312790cb75a872d34450b99903"
SRC_URI[sha256sum] = "751159fe19ce304abf7485bf2fbf12e569c060945c54588a7134ed8b18302347"
