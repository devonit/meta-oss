require fcitx.inc
inherit native

PR = "${INC_PR}.0"

DEPENDS += "extra-cmake-modules-native"

LIC_FILES_CHKSUM = "file://COPYING;md5=6306e547e5c5e43190193019ed46ee13"

SRC_URI += " \
    file://0001-build_only_tools.patch \
    file://fcitx-adjust-cmake-downloads.patch \
    "

EXTRA_OECMAKE += " \
    -DENABLE_X11=OFF \
    -DENABLE_GLIB2=OFF \
    -DENABLE_CAIRO=OFF \
    -DENABLE_DBUS=OFF \
    -DENABLE_PANGO=OFF \
    -DENABLE_LIBXML2=OFF \
    -DENABLE_DEBUG=OFF \
    -DENABLE_PINYIN=OFF \
    -DENABLE_TABLE=OFF \
    -DENABLE_GTK2_IM_MODULE=OFF \
    -DENABLE_GTK3_IM_MODULE=OFF \
    -DENABLE_QT=OFF \
    -DENABLE_QT_IM_MODULE=OFF \
    -DENABLE_QT_GUI=OFF \
    -DENABLE_OPENCC=OFF \
    -DFORCE_OPENCC=OFF \
    -DENABLE_LUA=OFF \
    -DENABLE_STATIC=OFF \
    -DENABLE_TEST=OFF \
    -DENABLE_SNOOPER=OFF \
    -DENABLE_GIR=OFF \
    -DENABLE_ENCHANT=OFF \
    -DENABLE_PRESAGE=OFF \
    -DFORCE_ENCHANT=OFF \
    -DFORCE_PRESAGE=OFF \
    -DENABLE_ICU=OFF \
    -DENABLE_BACKTRACE=OFF \
    -DENABLE_XDGAUTOSTART=OFF \
    -DENABLE_GETTEXT=OFF \
"

do_install_append() {
    install -d ${D}${bindir}
    install -m 755 ${OECMAKE_BUILDPATH}/tools/dev/fcitx-scanner ${D}${bindir}/fcitx-scanner
    install -m 755 ${OECMAKE_BUILDPATH}/tools/dev/fcitx-po-parser ${D}${bindir}/fcitx-po-parser
    install -m 755 ${OECMAKE_BUILDPATH}/src/module/spell/dict/comp-spell-dict ${D}${bindir}/comp-spell-dict
}

SRC_URI[md5sum] = "8cffd2c001ad0636af75107e20d90fb5"
SRC_URI[sha256sum] = "f15a3acbef40fa1f05c38e99a2334fe437e5ee938332c4914c9736b6b57aeeda"

