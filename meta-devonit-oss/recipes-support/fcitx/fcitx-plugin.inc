HOMEPAGE = "http://fcitx-im.org/"
LICENSE = "GPLv2"
DEPENDS += "fcitx fcitx-native"

INC_PR = "r0"

inherit pkgconfig cmake

SRC_URI= "https://github.com/fcitx/${PN}/archive/${PV}.tar.gz"

EXTRA_OECMAKE += " \
    -DOE_STAGING_DIR_TARGET=${STAGING_DIR_TARGET} \
    "

FILES_${PN} = " \
    /usr/share/icons/* \
    /usr/share/fcitx/* \
    /usr/lib/fcitx/*.so \
    "

FILES_${PN}-dbg = " \
    /usr/src/debug/* \
    /usr/lib/fcitx/.debug/* \
    "

