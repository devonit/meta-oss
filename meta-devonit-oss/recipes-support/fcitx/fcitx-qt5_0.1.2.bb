DESCRIPTION = "Fcitx Qt5 Input Context."
require fcitx-plugin.inc
inherit cmake_qt5
PR = "${INC_PR}.0"
LIC_FILES_CHKSUM = "file://README;md5=91f0e175f0fca8c9aaaa584a3c69f07b"

SRC_URI += "file://fcitx-qt5_install-to-correct-qt5-plugin-path.patch"

FILES_${PN} += " \
    /usr/lib/* \
    /usr/lib/pkgconfig/fcitx-qt5.pc \
    "

FILES_${PN}-dev += " \
    /usr/include/fcitx-qt5/* \
    /usr/lib/libfcitx-qt5.so \
    "

FILES_${PN}-dbg += " \
    /usr/lib/qt5/plugins/platforminputcontexts/.debug/* \
    /usr/lib/.debug/* \
    "

SRC_URI[md5sum] = "105b225e42ba1dc66fd114fe3a7601b2"
SRC_URI[sha256sum] = "7db70369bd0c7bd5da19dbe9a39e04e85b1c3f271d5b07fd13111aaaf236cae9"
