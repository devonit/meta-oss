#! /bin/sh

if [ -e "/etc/fcitx.profile" ]; then
    mkdir -p "$HOME/.config/fcitx/"
    cat "/etc/fcitx.profile" > "$HOME/.config/fcitx/profile"
fi

GTK_IM_MODULE=fcitx ; export GTK_IM_MODULE
QT_IM_MODULE=fcitx ; export QT_IM_MODULE
XMODIFIERS=@im=fcitx ; export XMODIFIERS
[ $(which fcitx) ] && fcitx --disable fcitx-xkb &
