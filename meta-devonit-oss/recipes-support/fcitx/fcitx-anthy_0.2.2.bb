DESCRIPTION = "Anthy wrapper for Fcitx."
require fcitx-plugin.inc
PR = "${INC_PR}.0"
LIC_FILES_CHKSUM = "file://COPYING;md5=00ed06f01bcb1983382068710eb131c4"

DEPENDS += "anthy"
RDEPENDS_${PN} += "anthy libanthy0"

SRC_URI[md5sum] = "2e4a195c52498da039735067ff8087f0"
SRC_URI[sha256sum] = "ec16c648f1e8b04fcddc61656574d8ab875f9515439aede6daf2afd8e2efa07a"

