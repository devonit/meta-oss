
EXTRA_OECONF = " \
    --disable-static \
    --disable-openct \
    --enable-pcsc \
    --disable-ctapi \
    --disable-doc \
"

do_install_append() {
    install -d ${D}${includedir}/libopensc
    install -m 644 ${S}/src/libopensc/*.h ${D}${includedir}/libopensc
    install -d ${D}${includedir}/common
    install -m 644 ${S}/src/common/*.h ${D}${includedir}/common
    install -d ${D}${includedir}/scconf
    install -m 644 ${S}/src/scconf/*.h ${D}${includedir}/scconf
    install -d ${D}${includedir}/pkcs11
    install -m 644 ${S}/src/pkcs11/*.h ${D}${includedir}/pkcs11
}

FILES_${PN}-dev += "\
    ${includedir}/libopensc \
"
