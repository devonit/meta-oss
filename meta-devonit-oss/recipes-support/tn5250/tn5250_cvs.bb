DESCRIPTION = "5250 telnet emulator for accessing an IBM iSeries (AS/400)"
LICENSE = "GPL"
LIC_FILES_CHKSUM = "file://COPYING;md5=68ad62c64cc6c620126241fd429e68fe"

inherit autotools

PR = "r0"

S = "${WORKDIR}/${PN}"

SRC_URI = " \
    cvs://anonymous@tn5250.cvs.sourceforge.net/cvsroot/tn5250;rev=1.9;module=${PN} \
    "

