DESCRIPTION = "5250 telnet emulator for accessing an IBM iSeries (AS/400)"
LICENSE = "GPL"
LIC_FILES_CHKSUM = "file://COPYING;md5=68ad62c64cc6c620126241fd429e68fe"

inherit autotools

PR = "r0"

S = "${WORKDIR}/${PN}-${PV}"

SRC_URI = " \
    https://downloads.sourceforge.net/project/${PN}/${PN}/${PV}/${PN}-${PV}.tar.gz \
    "

SRC_URI[md5sum] = "d1eb7c5a2e15cd2f43a1c115e2734153"
SRC_URI[sha256sum] = "354237d400dc46af887cb3ffa4ed1f2c371f5b8bee8be046a683a4ac9db4f9c5"

