DESCRIPTION = "faceless desktop manager"
SECTION = "x11"
LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://COPYING;md5=751419260aa954499f7abaabaa882bbe"
PR = "r1"

XUSERNAME ?= "xuser"

RDEPENDS_${PN} += "xinit pam-plugin-limits pam-plugin-env xuser-account"

S = "${WORKDIR}/git"
SRCREV ?= "c1e5d112bcecc7c07ca5993d7eb292c47df7a550"
PV = "0.11+git${SRCPV}"

SRC_URI = " \
             git://bitbucket.org/devonit/nodm.git;protocol=https \
             file://nodm.pam \
             file://Xwrapper.config \
             file://defaults \
          "

DEPENDS = "libpam systemd help2man-native"

inherit autotools systemd

RCONFLICTS_${PN} = "xserver-nodm-init"
RREPLACES_${PN} = "xserver-nodm-init"
RPROVIDES_${PN} = "xserver-nodm-init"

SYSTEMD_SERVICE_${PN} = "nodm.service"

FILES_${PN} += "/etc/pam.d /etc/X11 /etc/default"

do_install_append() {
    
    install -d ${D}/etc/pam.d
    install -m 755 ${WORKDIR}/nodm.pam ${D}/etc/pam.d/nodm

    install -d ${D}/etc/X11
    install -m 755 ${WORKDIR}/Xwrapper.config ${D}/etc/X11/Xwrapper.config

    install -d ${D}/etc/default
    install ${WORKDIR}/defaults ${D}/etc/default/nodm
    echo "NODM_USER=${XUSERNAME}" >> ${D}/etc/default/nodm

    # fixup service file for OE
    install -d ${D}/lib
    mv ${D}/usr/lib/systemd ${D}/lib
    rmdir ${D}/usr/lib
}

