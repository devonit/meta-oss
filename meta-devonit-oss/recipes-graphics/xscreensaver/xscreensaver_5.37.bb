SUMMARY = "Screen saver and locker for the X Window System"
DESCRIPTION = "Screen saver and locker for the X Window System"
HOMEPAGE = "https://www.jwz.org/xscreensaver/"
LICENSE = "BSD"
## LICENSE file missing in source!! BSD taken from Arch Linux PKGBUILD
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/BSD;md5=3775480a712fc46a69647678acb234cb"

DEPENDS = "libglade libxmu libglu libx11 gtk+ gdk-pixbuf intltool libxpm"

RDEPENDS_${PN} += " \
    perl-module-posix \
    perl-module-fcntl \
    perl-module-bytes \
    perl-module-digest-md5 \
"

PR = "r2"

inherit autotools

FILESEXTRAPATHS_prepend := "${THISDIR}/${PN}:"

SRC_URI = "\
    http://www.jwz.org/xscreensaver/${PN}-${PV}.tar.gz \
    file://0001-fix-cross-compilation.patch \
    file://0001-Remove-colorbars-and-xscreensaver-logo-for-BJC.patch \
    "
    
SRC_URI[md5sum] = "34ae54cf740c4f2627d2097ac06eef68"
SRC_URI[sha256sum] = "7ac7aea494df9293e68a4211b64861b577bbfb8769ee4b0a0f504cb27e6be5d9"

EXTRA_OECONF = "\
    --prefix=${D}/${prefix} \
    --libdir=${STAGING_LIBDIR} \
    --includedir=${STAGING_INCDIR} \
    --sysconfdir=/etc \
    --localstatedir=/var \
    --libexecdir=/usr/lib \
    --with-x-app-defaults=/usr/share/X11/app-defaults \
    --with-pam \
    --with-login-manager \
    --with-gtk \
    --with-gl \
    --without-gle \
    --with-pixbuf \
    --disable-root-passwd \
    --disable-nls \
    "

do_configure_prepend() {
    rm ${S}/aclocal.m4
}

do_compile() {
    oe_runmake all
}

do_install() {
    oe_runmake install_prefix=${D} install
}

PACKAGES += "\
    ${PN}-pixmap \
    ${PN}-glade \
    ${PN}-config \
    "


FILES_${PN} = "\
    /usr/bin/xscreensaver \
    /usr/bin/xscreensaver-command \
    /usr/bin/xscreensaver-getimage \
    /usr/bin/xscreensaver-getimage-file \
    /usr/bin/xscreensaver-getimage-video \
    /usr/bin/xscreensaver-gl-helper \
    /usr/bin/xscreensaver-text \
    /etc/pam.d/xscreensaver \
    "

PACKAGES += " \
    ${PN}-glslideshow \
    ${PN}-gltext \
"

FILES_${PN}-locale = " /usr/share/locale "
FILES_${PN}-pixmap = " /usr/share/pixmaps "
FILES_${PN}-glade  = " /usr/share/xscreensaver/glade "
FILES_${PN}-config = "\
    /usr/bin/xscreensaver-demo \
    /usr/share/X11/app-defaults/XScreenSaver \
    /usr/share/applications/xscreensaver-properties.desktop \
    /usr/share/xscreensaver/config \
     "

## auto-generate all the individual screensaver modules.
python populate_packages_prepend () {
    xsdir = d.expand('${libdir}/xscreensaver/')
    do_split_packages(d, xsdir, '^(.*)$',
                     'xscreensaver-%s', 'XScreenSaver: %s'
                     )
}

