SUMMARY = "xtrace - x11 server tracing proxy"
DESCRIPTION = "Xtrace fakes an X server and forwards all connections to a real X server, displaying the communication between the clients and the server in an (well, thoretically) human readable form."
PR = "r1"
LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://COPYING;md5=751419260aa954499f7abaabaa882bbe"

SRC_URI = "git://git.debian.org/xtrace/xtrace.git"

SRCREV = "0ab52b0c27c2d09b8f663edae787684f15584fd5"

DEPENDS = "xauth"

FILES_${PN} = "${bindir}/x11trace \
               ${datadir}/xtrace \
               "

S = "${WORKDIR}/git"

inherit autotools

do_install_append () {
    # Raname executable to x11trace since glibc owns xtrace
    mv ${D}/${bindir}/xtrace ${D}/${bindir}/x11trace
    # do the same to the man page
    mv ${D}/${mandir}/man1/xtrace.1 ${D}/${mandir}/man1/x11trace.1
}

