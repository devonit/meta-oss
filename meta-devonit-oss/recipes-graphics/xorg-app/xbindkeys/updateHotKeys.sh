#!/bin/sh

#merge xbindkeys config files into a single file
XBINDKEYSRC="/tmp/xbindkeysrc"
TMPFILE="/tmp/xbindkeysrc.tmp"
SYSTEM_CONF="/etc/xbindkeysrc"
SYSTEM_DIR="/etc/xbindkeysrc.d"
USER_DIR="${HOME}/.xbindkeysrc.d"

if [ -f "${SYSTEM_CONF}" ]; then
    cp "${SYSTEM_CONF}" "${TMPFILE}"
fi

if [ -d "${SYSTEM_DIR}" ]; then
    for FILE in $(ls -X "${SYSTEM_DIR}"); do
        cat "${SYSTEM_DIR}/${FILE}" >> "${TMPFILE}"
    done
fi

if [ -d "${USER_DIR}" ]; then
    for FILE in $(ls -X "${USER_DIR}"); do
        cat "${USER_DIR}/${FILE}" >> "${TMPFILE}"
    done
fi

mv "${TMPFILE}" "${XBINDKEYSRC}"
