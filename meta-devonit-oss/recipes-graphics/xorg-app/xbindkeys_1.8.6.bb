DESCRIPTION = "xbindkeys"
LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://COPYING;md5=24396300012ca5a98ff24b08179852a0"

DEPENDS += "libx11"

RDEPENDS_${PN} += "pm-utils"

inherit autotools

PR = "r6"

SRC_URI = " \
            http://www.nongnu.org/xbindkeys/xbindkeys-${PV}.tar.gz \
            file://01xbindkeys.sh \
            file://updateHotKeys.sh \
            "

SRC_URI[md5sum] = "c6ea9db56e075dae0697497e2ed390cc"
SRC_URI[sha256sum] = "6c0d18be19fc19ab9b4595edf3a23c0a6946c8a5eb5c1bc395471c8f9a710d18"

EXTRA_OECONF += " --disable-guile --prefix=/usr"

do_install_append() {
    install -d ${D}/etc/X11/Xsession.d
    install -m 755 ${WORKDIR}/01xbindkeys.sh ${D}/etc/X11/Xsession.d/01xbindkeys
    install -m 755 ${WORKDIR}/updateHotKeys.sh ${D}/usr/bin/updateHotKeys.sh
}
