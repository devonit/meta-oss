DESCRIPTION = "GNU Unifont - a Pan-Unicode X11 bitmap iso10646 font"
HOMEPAGE = "http://unifoundry.com/"
LICENSE = "czyborra|GPL-2|GPL-3|GPL-2-with-font-exception|public-domain"
LIC_FILES_CHKSUM = "file://README;md5=1201f3b00adee2fa4da7c07d9ae5fef6"

PACKAGES += "unifont-fonts"

SRC_URI = " \
     http://unifoundry.com/pub/debian/unifont-${PV}.tar.gz \
     file://arm-makefile.patch \
"

DEPENDS += "unifont-native"

PR = "r0.18"

SRC_URI[md5sum] = "73b9e5fbd754d1bc5f7b9ed210ebc50b"
SRC_URI[sha256sum] = "dd092fde596cbb8f70e28bee8253c6e226b6d740e7f57ded6158412813d1f5a6"

S = "${WORKDIR}/${PN}-${PV}"

do_compile_prepend() {
    sed -i "s#BINDIR = \$(CURDIR)/../bin#BINDIR = ${STAGING_DIR_NATIVE}${bindir}#"  ${S}/font/Makefile
    
}

do_compile() {
    oe_runmake  DESTDIR="${D}"
}

do_install() {
    oe_runmake install DESTDIR="${D}"
    install -m755 ${S}/font/precompiled/unifont.pcf.gz ${D}/usr/share/fonts/X11/misc/unifont.pcf.gz
}

FILES_${PN} = " \
    /usr/bin/ \
    /usr/share/man/ \
    /usr/share/unifont/ \
"

FILES_${PN}-fonts = " \
    /usr/share/fonts \
"

