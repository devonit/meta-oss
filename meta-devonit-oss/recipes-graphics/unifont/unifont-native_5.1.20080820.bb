DESCRIPTION = "GNU Unifont - a Pan-Unicode X11 bitmap iso10646 font"
HOMEPAGE = "http://unifoundry.com/"
LICENSE = "czyborra|GPL-2|GPL-3|GPL-2-with-font-exception|public-domain"
LIC_FILES_CHKSUM = "file://README;md5=1201f3b00adee2fa4da7c07d9ae5fef6"

PACKAGES += "unifont-fonts"

SRC_URI = " \
     http://unifoundry.com/pub/debian/unifont-${PV}.tar.gz \
     file://arm-makefile.patch \
"


SRC_URI[md5sum] = "73b9e5fbd754d1bc5f7b9ed210ebc50b"
SRC_URI[sha256sum] = "dd092fde596cbb8f70e28bee8253c6e226b6d740e7f57ded6158412813d1f5a6"

inherit native

PR = "r1.6"
S = "${WORKDIR}/unifont-${PV}"



do_compile() {
    oe_runmake  DESTDIR="${SYSROOT_DESTDIR}/${SYSROOT_DESTDIR}"
}

do_install() {
    #oe_runmake install DESTDIR="${SYSROOT_DESTDIR}/"
    install -d ${SYSROOT_DESTDIR}/${bindir}
    install -m755 ${S}/bin/bdfimplode ${SYSROOT_DESTDIR}/${bindir}
    install -m755 ${S}/bin/hex2bdf ${SYSROOT_DESTDIR}/${bindir}
    install -m755 ${S}/bin/hex2bdf-split ${SYSROOT_DESTDIR}/${bindir}
    install -m755 ${S}/bin/hex2sfd ${SYSROOT_DESTDIR}/${bindir}
    install -m755 ${S}/bin/hexbraille ${SYSROOT_DESTDIR}/${bindir}
    install -m755 ${S}/bin/hexdraw ${SYSROOT_DESTDIR}/${bindir}
    install -m755 ${S}/bin/hexmerge ${SYSROOT_DESTDIR}/${bindir}
    install -m755 ${S}/bin/johab2ucs2 ${SYSROOT_DESTDIR}/${bindir}
    install -m755 ${S}/bin/unibmp2hex ${SYSROOT_DESTDIR}/${bindir}
    install -m755 ${S}/bin/unicoverage ${SYSROOT_DESTDIR}/${bindir}
    install -m755 ${S}/bin/unidup ${SYSROOT_DESTDIR}/${bindir}
    install -m755 ${S}/bin/unihex2bmp ${SYSROOT_DESTDIR}/${bindir}
    install -m755 ${S}/bin/unipagecount ${SYSROOT_DESTDIR}/${bindir}
    install -m755 ${S}/bin/uniunmask ${SYSROOT_DESTDIR}/${bindir}

}

FILES_${PN} = " \
    /usr/bin/ \
    /usr/share/man/ \
    /usr/share/unifont/ \
"

FILES_${PN}-fonts = " \
    /usr/share/fonts \
"

