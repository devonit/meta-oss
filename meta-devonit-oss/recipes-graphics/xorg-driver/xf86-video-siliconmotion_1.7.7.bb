require recipes-graphics/xorg-driver/xorg-driver-video.inc

LIC_FILES_CHKSUM = "file://COPYING;md5=3893e77db70569921f6d79c387b5748a"

DESCRIPTION = "X.Org X server -- S3 display driver"
DEPENDS += "virtual/libx11 libxvmc drm glproto xf86driproto \
            virtual/libgl xineramaproto libpciaccess"

COMPATIBLE_HOST = '(i.86|x86_64).*-linux'

PE = "1"
PR = "1"

SRC_URI[md5sum] = "dc139f8ea17c40fb7bcc89181e9dbfb3"
SRC_URI[sha256sum] = "87b8b59d43945d4fc8012860c0bd9aed42c4684a943355c607b8eb8d6710c3aa"

