require recipes-graphics/xorg-driver/xorg-driver-video.inc

LIC_FILES_CHKSUM = "file://COPYING;md5=cbbdd887d04deb501076c22917e2030d"

DESCRIPTION = "X.Org X server -- SIS display driver"
DEPENDS += "virtual/libx11 \
            libxvmc \
            drm \
            glproto \
            xf86driproto \
            virtual/libgl \
            xineramaproto \
            libpciaccess \
            xserver-xorg \
            xproto \
            fontsproto \
            xf86dgaproto \
           "

SRC_URI += " \
    file://sis-configure.patch \
"
#             file://sis-updated-xorg.patch

EXTRA_OECONF += "--enable-dri"
           
COMPATIBLE_HOST = '(i.86|x86_64).*-linux'

PR = "1"

SRC_URI[md5sum] = "c173c4ce572eb19db5dfdc8a858c6c67"
SRC_URI[sha256sum] = "c8f3f2577f69334dfcc4bf96477dce45161170555f3abdfa105599e61bc7d3fe"

