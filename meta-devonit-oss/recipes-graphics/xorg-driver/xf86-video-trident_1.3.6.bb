require recipes-graphics/xorg-driver/xorg-driver-video.inc

LIC_FILES_CHKSUM = "file://COPYING;md5=2e9eb6db89324a99415a93a059157da7"

DESCRIPTION = "X.Org X server -- Trident display driver"
DEPENDS += "virtual/libx11 libxvmc drm glproto xf86driproto \
            virtual/libgl xineramaproto libpciaccess"

COMPATIBLE_HOST = '(i.86|x86_64).*-linux'

SRC_URI += "file://trident-remove-mibstore.patch"

PR = "1"

SRC_URI[md5sum] = "86fed7e2b2876cb349f958cdd8319118"
SRC_URI[sha256sum] = "6a58e3f3034abd8803af8a5c7dd5a6a4a28ed4fdac742ffb05518caaddc28104"
