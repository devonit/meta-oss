require recipes-graphics/xorg-driver/xorg-driver-video.inc

LIC_FILES_CHKSUM = "file://COPYING;md5=7afbe929192be6cccb50f81b968fd23a"

DESCRIPTION = "X.Org X server -- R128 display driver"
DEPENDS += "virtual/libx11 libxvmc drm glproto xf86driproto \
            virtual/libgl xineramaproto libpciaccess"

COMPATIBLE_HOST = '(i.86|x86_64).*-linux'

SRC_URI += "file://r128-configure.patch"

PR = "1"

SRC_URI[md5sum] = "770c2342141874f8a81a95ecd1839746"
SRC_URI[sha256sum] = "5ebfef49831c9b12f7b7011c8314010596ac2ab0d5b9b7cfd17908e93d7de4ea"

