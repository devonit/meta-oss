require recipes-graphics/xorg-driver/xorg-driver-video.inc

LIC_FILES_CHKSUM = "file://COPYING;md5=09743e0f5c076a765cd16697b5b1effb"

DESCRIPTION = "X.Org X server -- S3 display driver"
DEPENDS += "virtual/libx11 libxvmc drm glproto xf86driproto \
            virtual/libgl xineramaproto libpciaccess"

COMPATIBLE_HOST = '(i.86|x86_64).*-linux'

PE = "1"
PR = "1"

SRC_URI[md5sum] = "9d4dd8060544d95a1ce7d0ce0853cbe6"
SRC_URI[sha256sum] = "5dbe68de05483f902fdc48e97ce8d9fdd1d2ade14cb53c0c3642f0259f65a4da"

