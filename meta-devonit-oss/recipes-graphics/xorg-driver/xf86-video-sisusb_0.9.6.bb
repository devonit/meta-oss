require recipes-graphics/xorg-driver/xorg-driver-video.inc

LIC_FILES_CHKSUM = "file://COPYING;md5=b08997efa10dc31f51dad7e85e77f182"

DESCRIPTION = "X.Org X server -- S3 display driver"
DEPENDS += "virtual/libx11 libxvmc drm glproto xf86driproto \
            virtual/libgl xineramaproto libpciaccess"

SRC_URI += "file://sisusb-remove-mibstore.patch"

COMPATIBLE_HOST = '(i.86|x86_64).*-linux'

PE = "1"
PR = "1"

SRC_URI[md5sum] = "8ebcd2bc5a87a3f1362fa76616a83e57"
SRC_URI[sha256sum] = "73dbef43c56a4ce1445c27ebac2ddc062c643c32ca6e2a4d095aea2185b9e046"

