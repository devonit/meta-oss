require recipes-graphics/xorg-driver/xorg-driver-video.inc

LIC_FILES_CHKSUM = "file://COPYING;md5=1f50f1289ca3b91a542a26ba5df51608"

DESCRIPTION = "X.Org X server -- S3 display driver"
DEPENDS += "virtual/libx11 libxvmc drm glproto xf86driproto \
            virtual/libgl xineramaproto libpciaccess"

COMPATIBLE_HOST = '(i.86|x86_64).*-linux'

SRC_URI += "file://savage-configure.patch"

PR = "1"

SRC_URI[md5sum] = "e813271ab43cc6a95ac0ab252b90a885"
SRC_URI[sha256sum] = "041d4205c9222c1780fba6e0e397a559aed393b7a7991b58fa79ba8cccc54a44"

