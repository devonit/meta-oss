DESCRIPTION = "DisplayLink driver for Linux"
HOMEPAGE = "http://www.displaylink.com/downloads/ubuntu"
LICENSE = "Proprietary"
LIC_FILES_CHKSUM = "file://${PN}-driver-${PV}/LICENSE;md5=d4c00d6f729d6d092d1dd2d528b5212f"
PR = "r1"

inherit systemd
SYSTEMD_SERVICE_${PN} = "${PN}.service"
SYSTEMD_PACKAGES += "${PN}"

RDEPENDS_${PN} = "xf86-video-modesetting"

DL_URL = "http://www.displaylink.com/downloads/file?id=744"
SRC_URI = " \
    ${DL_URL};downloadfilename=${PN}_${PV}.zip \
    file://99-displaylink.rules \
    file://displaylink-sleep.sh \
    file://displaylink.service \
"

SRC_URI[md5sum] = "2df66116bdf2f6f0fd14fb492d63abc5"
SRC_URI[sha256sum] = "1364380fbc5774e5ab5c2744f65bb3de10850538f8520baadfc11cc7babe2d66"

S = "${WORKDIR}"
PACK = "${WORKDIR}/${PN}-driver-${PV}"

# add an extra task to get the extracted contents
do_unpack_extra () {
    chmod +x ${PACK}.run
    ${PACK}.run --keep --noexec
}
addtask unpack_extra after do_unpack before do_patch

do_install () {
    COREDIR="/opt/${PN}"
    install -d ${D}${COREDIR}
    install -d ${D}${systemd_unitdir}/system
    install -d ${D}${systemd_unitdir}/system-sleep
    install -d ${D}${sysconfdir}/udev/rules.d
    install -d -m750 ${D}/var/log/${PN}

    install -m644 ${WORKDIR}/99-displaylink.rules ${D}${sysconfdir}/udev/rules.d/99-displaylink.rules

    install -m644 ${WORKDIR}/displaylink.service ${D}${systemd_unitdir}/system/displaylink.service
    install -m755 ${WORKDIR}/displaylink-sleep.sh ${D}${COREDIR}/displaylink-sleep.sh
    ln -sf ${COREDIR}/displaylink-sleep.sh  ${D}${systemd_unitdir}/system-sleep/displaylink-sleep.sh
    
    install -m644 ${PACK}/ella-dock-release.spkg ${D}${COREDIR}/ella-dock-release.spkg
    install -m644 ${PACK}/firefly-monitor-release.spkg ${D}${COREDIR}/firefly-monitor-release.spkg
    install -m644 ${PACK}/ridge-dock-release.spkg ${D}${COREDIR}/ridge-dock-release.spkg
}

do_install_append_x86 () {
    install -m755 ${PACK}/x86-ubuntu-1604/DisplayLinkManager ${D}${COREDIR}/DisplayLinkManager
    install -m755 ${PACK}/x86-ubuntu-1604/libusb-1.0.so.0.1.0 ${D}${COREDIR}/libusb-1.0.so.0
    install -m755 ${PACK}/x86-ubuntu-1604/libevdi.so ${D}${COREDIR}/libevdi.so
}

do_install_append_x86-64 () {
    install -m755 ${PACK}/x64-ubuntu-1604/DisplayLinkManager ${D}${COREDIR}/DisplayLinkManager
    install -m755 ${PACK}/x64-ubuntu-1604/libusb-1.0.so.0.1.0 ${D}${COREDIR}/libusb-1.0.so.0
    install -m755 ${PACK}/x64-ubuntu-1604/libevdi.so ${D}${COREDIR}/libevdi.so
}

FILES_${PN} = " \
    /opt/displaylink \
    ${sysconfdir} \
    ${systemd_unitdir} \
    ${localstatedir} \
"

