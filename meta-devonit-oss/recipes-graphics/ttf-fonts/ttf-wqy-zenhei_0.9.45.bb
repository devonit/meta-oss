require recipes-graphics/ttf-fonts/ttf.inc

DESCRIPTION = "WenQuanYi Zen Hei - A Hei-Ti Style Chinese font"
AUTHOR = "Qianqian Fang and The WenQuanYi Project Contributors"
HOMEPAGE = "http://wqy.sourceforge.net/en/"
LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://${COREBASE}/meta/files/common-licenses/GPL-2.0;md5=801f80980d171dd6425610833a22dbe6"
PR = "r1"

SRC_URI = "${SOURCEFORGE_MIRROR}/wqy/wqy-zenhei-${PV}.tar.gz"
S = "${WORKDIR}/wqy-zenhei"

do_install_append () {
        install -d ${D}${sysconfdir}/fonts/conf.d/
        install -m 0644 ${S}/44-wqy-zenhei.conf ${D}${sysconfdir}/fonts/conf.d/
        install -m 0644 ${S}/43-wqy-zenhei-sharp.conf ${D}${sysconfdir}/fonts/conf.d/
}

PACKAGES = "${PN}"

FILES_${PN} = "${datadir}/fonts ${sysconfdir}"

SRC_URI[md5sum] = "4c6c3f4e902dd5ee0a121e8c41d040bd"
SRC_URI[sha256sum] = "e4b7e306475bf9427d1757578f0e4528930c84c44eaa3f167d4c42f110ee75d6"
