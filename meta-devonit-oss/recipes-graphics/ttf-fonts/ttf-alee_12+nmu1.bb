require recipes-graphics/ttf-fonts/ttf.inc

DESCRIPTION = "Set of free Hangul TrueType fonts"
LICENSE = "Artistic-1.0"
LIC_FILES_CHKSUM = "file://COPYING;md5=136cf2a8efe0f4565c20dccb9f6c3e8a"
PR = "r2"

SRC_URI = "ftp://ftp.zcu.cz/pub/linux/debian/pool/main/t/ttf-alee/${PN}_${PV}.tar.gz"

FILES_${PN} = "${datadir}"

SRC_URI[md5sum] = "dcf7e9d86e68b56352f518fe258fd144"
SRC_URI[sha256sum] = "a517b17f860a9689061b8439c1fad295d1b7aa201695897158ec6efdaa04e83c"

