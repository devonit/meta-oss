inherit autotools pkgconfig

DESCRIPTION="Fast terminal emulator for the Linux framebuffer"
HOMEPAGE="http://fbterm.googlecode.com/"
SRC_URI="http://fbterm.googlecode.com/files/${PN}-${PV}.tar.gz"
LICENSE="GPLv2"
LIC_FILES_CHKSUM = "file://COPYING;md5=d8e20eece214df8ef953ed5857862150"

PR = "r2"

DEPENDS="\
         fontconfig \
         freetype \
         ncurses \
        "

RDEPENDS_${PN} += "\
             ttf-dejavu-sans \
             ttf-dejavu-sans \
             ttf-dejavu-sans-mono \
             ttf-sazanami-gothic \
             ttf-sazanami-mincho \
             ttf-arphic-uming \
             ttf-alee \
             ttf-bitstream-vera \
            "

S = "${WORKDIR}/${PN}-1.7"
B = "${S}"
        
SRC_URI[md5sum] = "c36bae75a450df0519b4527cccaf7572"
SRC_URI[sha256sum] = "720f600817217257aa4c822e67814495dcb6c8d6326cdde5fe3ba1e457d9915d"
