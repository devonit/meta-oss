
EXTRA_OECONF += "--enable-xephyr --enable-kdrive"
PACKAGECONFIG += "udev glx dri dri2 dri3 glamor systemd-logind xinerama xshmfence"

DEPENDS += " \
    xcb-util-wm \
    xcb-util-keysyms \
    xcb-util-image \
    xcb-util-renderutil \
"

