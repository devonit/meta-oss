FILESEXTRAPATHS_prepend := "${THISDIR}/gcc-5.3:"

SRC_URI += " \
    file://0001-i386-Move-struct-ix86_frame-to-machine_function.patch;striplevel=2 \
    file://0002-i386-Use-reference-of-struct-ix86_frame-to-avoid-cop.patch;striplevel=2 \
    file://0003-i386-More-use-reference-of-struct-ix86_frame-to-avoi.patch;striplevel=2 \
    file://0004-i386-Don-t-use-reference-of-struct-ix86_frame.patch;striplevel=2 \
    file://0005-x86-Add-mindirect-branch-doc.patch;striplevel=2 \
    file://0005-x86-Add-mindirect-branch.patch;striplevel=2 \
    file://0006-x86-Add-mfunction-return-doc.patch;striplevel=2 \
    file://0006-x86-Add-mfunction-return.patch;striplevel=2 \
    file://0007-x86-Add-mindirect-branch-register-doc.patch;striplevel=2 \
    file://0007-x86-Add-mindirect-branch-register.patch;striplevel=2 \
    file://0008-x86-Add-V-register-operand-modifier-doc.patch;striplevel=2 \
    file://0008-x86-Add-V-register-operand-modifier.patch;striplevel=2 \
    file://0009-x86-Disallow-mindirect-branch-mfunction-return-with-.patch;striplevel=2 \
    file://0009-x86-Disallow-mindirect-branch-mfunction-return-with-doc.patch;striplevel=2 \
"

#    file://ppc-add-mspeculate-indirect-jumps.patch;striplevel=2
