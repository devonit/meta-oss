DESCRIPTION = "An image initramfs using systemd"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/meta/COPYING.MIT;md5=3da9cfbcb788c80a0384361b4de20420"
RDEPENDS_${PN} += "systemd aufs-util udev"
RRECOMMENDS_${PN} += "kernel-module-aufs kernel-module-loop kernel-module-squashfs"

PR = "r1"

do_install() {
    # set systemd as init
    #ln -s /lib/systemd/systemd ${D}/init
    # SEE: IMAGE_INIT_MANAGER = "systemd"
    
    # let systemd know that it is in initrd mode.
    # http://www.freedesktop.org/wiki/Software/systemd/InitrdInterface/
    install -d ${D}/etc
    touch ${D}/etc/initrd-release
    
    # create the obvious, to be sure.
    install -d ${D}/dev
    install -d ${D}/proc
    install -d ${D}/run
    install -d ${D}/sys
    install -d ${D}/mnt
    install -d ${D}/tmp
    install -d ${D}/var/lock
    
    # mount point of rootfs for switch_root
    install -d ${D}/sysroot
}

FILES_${PN} += "\
    /etc \
    /dev \
    /lib \
    /mnt \
    /proc \
    /run \
    /sys \
    /tmp \
    /var \
    /sysroot \
    "

# Due to kernel depdendency
PACKAGE_ARCH = "${MACHINE_ARCH}"
