DESCRIPTION = "Backports of select Qt5 components compiled for Qt4."
LICENSE = "LGPLv2.1"

inherit qmake2 qt4x11

LIC_FILES_CHKSUM = "file://${COREBASE}/meta/files/common-licenses/LGPL-2.1;md5=1a6d268fd218675ffea8be556788b780"

QMAKE_PROFILES = "${PN}.pro"

PR = "r1"
PKGV = "1.0.0+gitr${GITPKGV}"
SRCREV = "${AUTOREV}"

SRC_URI = " \
    git://git@bitbucket.org/devonit/${PN}.git;protocol=ssh \
"

S = "${WORKDIR}/git"

EXTRA_QMAKEVARS_PRE += "PREFIX=/usr"

do_install () {
    oe_runmake install INSTALL_ROOT="${D}"
}

