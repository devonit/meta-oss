DESCRIPTION = "QAMQP is a Qt implementation of the AMQP 0-9-1 protocol."
HOMEPAGE = "https://github.com/mbroadst/qamqp"
LICENSE = "LGPLv2.1"
PR = "r2"
PV = "git${SRCPV}"

inherit qmake2 qt4x11

LIC_FILES_CHKSUM = "file://LICENSE;md5=cd76628474cded3932853f294842b0bc"

QMAKE_PROFILES = "qamqp.pro"

SRCREV = "7d9f80331e66119fa4531db46d30372c47ed1510"
SRC_URI = "git://github.com/mbroadst/qamqp.git;protocol=https"

S = "${WORKDIR}/git"

EXTRA_QMAKEVARS_PRE += "PREFIX=/usr"

do_install () {
    oe_runmake install INSTALL_ROOT="${D}"

    #fix pkgconfig for the sysroot
    sed 's#/mnt/bitbake/build/detos/tmp-glibc/sysroots/x86_64-linux##' -i ${D}/${libdir}/pkgconfig/*.pc
}

