DESCRIPTION = "QJsonRpc is a Qt implementation of the JSON-RPC protocol."
HOMEPAGE = "https://bitbucket.org/devonit/qjsonrpc"
LICENSE = "LGPLv2.1"
PR = "r8"
PV = "1.0+git${SRCPV}"

inherit qmake2 qt4x11 gitpkgv

LIC_FILES_CHKSUM = "file://LICENSE;md5=d5f2a6dd66c44b3907034c2016dc5a5e"

QMAKE_PROFILES = "qjsonrpc.pro"

SRC_URI = "\
    git://git@bitbucket.org/devonit/qjsonrpc.git;protocol=ssh \
    file://0000-remove-tests.patch \
    "
# https://bitbucket.org/devonit/qjsonrpc/src/b138256fe2b8d5933c9aafd91dc067c45e6cf4ff/?at=v1.0.1
SRCREV = "v1.1.0"

S = "${WORKDIR}/git"

EXTRA_QMAKEVARS_PRE += "PREFIX=/usr"

do_install () {
    oe_runmake install INSTALL_ROOT="${D}"
}

FILES_${PN} = "/usr/lib"
