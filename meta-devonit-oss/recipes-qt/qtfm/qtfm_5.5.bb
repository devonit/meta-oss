DESCRIPTION = "Qt File Manager"
LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://COPYING;md5=94d55d512a9ba36caa9b7df079bae19f"

inherit qmake2 qt4x11 gitpkgv

DEPENDS += "file"

PR = "r1"

SRCREV = "${PN}-${PV}"

S = "${WORKDIR}/git"

SRC_URI = " \
    git://bitbucket.org/devonit/qtfm.git;protocol=https;branch=${PV} \
    file://compile-qm.patch \
    "

do_install () {
    oe_runmake install INSTALL_ROOT="${D}"
}
