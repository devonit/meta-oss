FILESEXTRAPATHS_prepend := "${THISDIR}/files:"

DEPENDS += "cups gtk+ gstreamer"

QT_CONFIG_FLAGS += "-cups"
QT_X11_FLAGS = "-xinerama -xkb -gtkstyle"
QT_PHONON = ""

