DESCRIPTION = "Qt version of dialog"
LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://${COREBASE}/meta/files/common-licenses/GPL-2.0;md5=801f80980d171dd6425610833a22dbe6"

RDEPENDS_${PN} = " \
             qt4-plugin-imageformat-svg \
             qt4-plugin-imageformat-jpeg  \
             qt4-plugin-iconengine-svgicon \
           "

inherit qmake2 qt4x11

QMAKE_PROFILES = "${PN}.pro"

PR = "r1"
PV = "git${SRCPV}"
SRCREV = "1eddc38fb74d1706b5b13ccae1e48d32226bc6a8"
SRC_URI = "git://git@bitbucket.org/devonit/qdialog.git;protocol=ssh"

S = "${WORKDIR}/git"

do_install () {
    oe_runmake install INSTALL_ROOT="${D}"
}

