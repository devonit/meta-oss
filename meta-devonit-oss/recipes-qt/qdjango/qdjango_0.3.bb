DESCRIPTION = "QDjango"
HOMEPAGE = "https://bitbucket.org/devonit/qdjango"
LICENSE = "LGPLv2.1"
PR = "r3"
PV = "0.3+git${SRCPV}"

inherit qmake2 qt4x11 gitpkgv

LIC_FILES_CHKSUM = "file://README;md5=5a2d50472adca85126ee9659dfac21c1"

QMAKE_PROFILES = "qdjango.pro"

# https://bitbucket.org/devonit/qdjango/src/9a01acd3989ee736527d5e4d517706cf8d8855d2/?at=v0.3.0
SRCREV = "9a01acd3989ee736527d5e4d517706cf8d8855d2"

SRC_URI = "\
    git://git@bitbucket.org/devonit/qdjango.git;protocol=ssh \
    file://0001-only-build-db-and-skip-tests-and-examples.patch \
    "

S = "${WORKDIR}/git"

EXTRA_QMAKEVARS_PRE += "PREFIX=/usr"

do_install () {
    oe_runmake install INSTALL_ROOT="${D}"
}
