DESCRIPTION = "QDjango"
HOMEPAGE = "https://bitbucket.org/devonit/qdjango"
LICENSE = "LGPLv2.1"
PR = "r3"

inherit qmake2 qt4x11 gitpkgv

LIC_FILES_CHKSUM = "file://README;md5=5a2d50472adca85126ee9659dfac21c1"

QMAKE_PROFILES = "qdjango.pro"

SRCREV = "${AUTOREV}"
PV = "${SRCPV}"
PKGV = "${GITPKGV}"
DEFAULT_PREFERENCE = "-1"

SRC_URI = "\
    git://git@bitbucket.org/devonit/qdjango.git;protocol=ssh \
    file://0001-only-build-db-and-skip-tests-and-examples.patch \
    "

S = "${WORKDIR}/git"

EXTRA_QMAKEVARS_PRE += "PREFIX=/usr"

do_install () {
    oe_runmake install INSTALL_ROOT="${D}"
}
