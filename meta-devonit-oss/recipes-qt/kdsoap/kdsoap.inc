DESCRIPTION = "KD Soap is a Qt-based client-side and server-side SOAP component."
HOMEPAGE = "http://www.kdab.com/kdab-products/kd-soap"
LICENSE = "LGPLv2.1|GPLv2|KDAB"
LIC_FILES_CHKSUM = "file://LICENSE.txt;md5=f87fbbd468f778d35310b15b3d03d219"

inherit qt4x11

QT4DEPENDS = "qt4-x11"
QT4DEPENDS_class-nativesdk = " "
QT4DEPENDS_class-native = " "
DEPENDS += "python-native qt4-native"

SRC_URI += " \
             git://github.com/KDAB/KDSoap.git \
          "

S = "${WORKDIR}/git"

EXTRA_QMAKEVARS_POST += "-recursive"

export QMAKESPEC_class-nativesdk = "${STAGING_DATADIR_NATIVE}/qt4/mkspecs/${TARGET_OS}-oe-g++"
export OE_QMAKE_INCDIR_QT_class-nativesdk = "${STAGING_INCDIR_NATIVE}/qt4"
export OE_QMAKE_LIBDIR_QT_class-nativesdk = "${STAGING_LIBDIR_NATIVE}"

do_configure_prepend () {
    QTDIR=/usr python autogen.py -prefix /usr -release -no-unittests -shared
}

SRC_URI += "file://remove_examples.patch"

do_install () {
   oe_runmake INSTALL_ROOT="${D}${base_prefix}" install
   install -d ${D}${datadir}/qt4
   mv ${D}${base_prefix}/usr/mkspecs ${D}${datadir}/qt4/mkspecs
   mv ${D}${base_prefix}/usr/*.pri ${D}${datadir}/qt4/mkspecs
}

do_install_class-nativesdk () {
   install -d ${D}${bindir}
   install -m 755 bin/kdwsdl2cpp ${D}${bindir}
}

PACKAGES += "${PN}-kdwsdl2cpp"

FILES_${PN} = "${libdir}/*${SOLIBS}"
FILES_${PN}-dev += "${datadir}/qt4/mkspecs"
FILES_${PN}-kdwsdl2cpp = "${bindir}/kdwsdl2cpp"

do_sync_submodules() {
        git submodule update --init
}

addtask sync_submodules before do_patch after do_unpack

BBCLASSEXTEND = "native nativesdk"

